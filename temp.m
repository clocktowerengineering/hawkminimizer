

    clear vals
    for i = 1:length(fixedppspoints)
      temp = rt_to_IEC61217_Deq0_params(generate_IEC61217_Deq0(newbasis(1:6)) * fixedppspoints(i).aquilaRaw);
      vals(i,1) = temp(1);
      vals(i,2) = temp(2);
      vals(i,3) = temp(3);
      vals(i,4) = temp(4);
      vals(i,5) = temp(5);
    endfor


    figure('Name',[datestr(now)," ",mat2str(newbasis)])
    subplot(3,1,1)
    plot(vals(:,1))
    legend('x val')
    ylabel('value in m')
    subplot(3,1,2)

    plot(vals(:,2))
    legend('y val')
    ylabel('value in m')
    subplot(3,1,3)

    plot(,vals(:,3))
    legend('z val')
    ylabel('value in m')
    
   obj = sqrt((max(vals(:,5))-min(vals(:,5)))^2+(max(vals(:,4))-min(vals(:,4)))^2+(max(vals(:,1))-min(vals(:,1)))^2+(max(vals(:,2))-min(vals(:,2)))^2+(max(vals(:,3))-min(vals(:,3)))^2);
    
    