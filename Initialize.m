clc
addpath(genpath('.'));

output_precision(10);
camerabasisshift = [0,0,0,0,0,0]
## step1: make sure that the machine this is running from has a key installed on the mcs server.
## see http://www.linuxproblem.org/art_9.html for more infos.

# TODO: add some sort of checking to see if we can log in without requiring a login.
# change the below line to point to your ssh key.
pointsrequired = 30;

sshline = ['ssh -i ',pwd,'\id_rsa root@192.168.102.100'];

[orion_tooltip,orion_isocenter] = getOrionConfig (sshline);
[hawk_tooltip] = getHawkConfig (sshline);

[aquilacameraposition,falcocameraposition]= getCameraPosition(sshline);

structname = 'fixedppspositions';
cameraselector = 'aquilaIEC';