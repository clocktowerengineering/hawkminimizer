    
  clear fixedppspoints
  fixedppspoints = removeZerosFromStruct(capturedpoints,'aquila',cleanup=0,removezeroerror=true);
  # ok now falco
  
  fixedppspoints = removeZerosFromStruct(fixedppspoints,'falco',cleanup=0,removezeroerror=true);
  
  for i = 1: length(fixedppspoints)
    fixedppspoints(i).aquilaRaw = aquilacameraposition^-1 * fixedppspoints(i).aquila;
    fixedppspoints(i).falcoRaw = falcocameraposition^-1 * fixedppspoints(i).falco;
  endfor
  
  
  
    

    clear vals
    for i = 1:length(fixedppspoints)
      temp = rt_to_IEC61217_Deq0_params(generate_IEC61217_Deq0(newbasis(1:6)) * fixedppspoints(i).aquilaRaw);
      temp2 = rt_to_IEC61217_Deq0_params(generate_IEC61217_Deq0(fixedppspoints(i).correctedppsposition)*generate_IEC61217_Deq0(newbasis(7:12)));
      
      vals(i,1) = (temp(1)-temp2(1));
      vals(i,2) = (temp(2)-temp2(2));
      vals(i,3) = (temp(3)-temp2(3));
      vals(i,4) = (temp(4)-temp2(4));
      vals(i,5) = (temp(5)-temp2(5));
      vals(i,6) = (temp(6)-temp2(6));
    endfor


    figure('Name',[datestr(now)," ",mat2str(newbasis)])
    subplot(6,1,1)
    plot(vals(:,1))
    legend('x val')
    ylabel('value in m')
    subplot(6,1,2)

    plot(vals(:,2))
    legend('y val')
    ylabel('value in m')
    subplot(6,1,3)

    plot(vals(:,3))
    legend('z val')
    ylabel('value in m')
    
    
    subplot(6,1,4)
    plot(vals(:,4))
    legend('rx val')
    ylabel('value in rad')
    subplot(6,1,5)

    plot(vals(:,5))
    legend('ry val')
    ylabel('value in rad')
    subplot(6,1,6)

    plot(vals(:,6))
    legend('rz val')
    ylabel('value in rad')
    
    figure('Name',[datestr(now)," ",mat2str(newbasis)])
    plot(sqrt(vals(:,1).^2+vals(:,2).^2+vals(:,3).^2))