%% HawkInit
%clear
clc

output_precision(10);

%% step1: make sure that the machine this is running from has a key installed on the mcs server.
%% see http://www.linuxproblem.org/art_9.html for more infos.

% TODO: add some sort of checking to see if we can log in without requiring a login.
% change the below line to point to your ssh key.
pointsrequired = 300;

[orion_tooltip,orion_isocenter] = getOrionConfig (sshline);
[hawk_tooltip] = getHawkConfig (sshline);
initial = ppsGetPosition(sshline);

[aquilacameraposition,falcocameraposition]= getCameraPosition(sshline);
correctwhichcamera = 'falco';

acquiring = false;

disp 'press SHIFT+A to automatically go through a preplanned point cloud.'
disp 'press SHIFT+T to take new points manually.'
disp 'any other keys will use existing point cloud.'
newpoints = kbhit();

if newpoints == 'A'
  % call the pps autocapture function.
  
  %generate ppscloud.
  initial = ppsGetPosition(sshline);
  generatedppspositions = ppsPositionGenerator(squaredistance=.15,squaresteps=5,heightdistance=.15,heightsteps=2,pitchrolljitter = false, angles=[0,pi/8,3*pi/8,pi/2],initial);
  successfulpoints = 0;
  for indexvar = 1:length(generatedppspositions)
    disp(['going to position ',mat2str(indexvar),'/',mat2str(length(generatedppspositions)),'. Successful points so far:',mat2str(successfulpoints)]);
    [ppsposition,atposition,positionerror] = ppsToPositionBlocking(sshline,generatedppspositions(indexvar,:));
    
    %move succeeded.  
    if atposition
      
      %check the camerastatus.
      
      [status,curloutput] = hawkcURLer(sshline);
      [aquilaposition,falcoposition,status,aquilaerror,falcoerror] = parseHawk(curloutput);
      
      if ~status
        disp 'Bad position, couldn''t see one or both cameras.  moving to next point.'
      endif
        
     

        capturedpoints(indexvar).aquila = aquilaposition;
        capturedpoints(indexvar).falco = falcoposition;
        %grab the pps position (yeah, it's offset by tooltip, shaddap.)
        capturedpoints(indexvar).ppsposition = ppsposition;
        ppsposition(7) = ppsposition(4);
        ppsposition(4) = [];
        capturedpoints(indexvar).correctedppsposition = ppsposition;
        capturedpoints(indexvar).positionerror = positionerror;
        capturedpoints(indexvar).aquilaerror = aquilaerror;
        capturedpoints(indexvar).falcoerror = falcoerror;
        capturedpoints(indexvar).status = status;     

    
    else
      %move failed. go to next point.
      disp 'Move index failed.  going to next point.'
    endif
  endfor
  
  % alright, let's translate these correctly
  

elseif newpoints == 'T'  

  acquiring = true;
  indexvar = 1;
end

while acquiring
  disp 'Press any key OTHER THAN (SHIFT+Q) when you want to capture the camera positions.'
  disp ' '

  um = kbhit();
  if um == 'Q'
    acquiring = false
  end
  
   

  [status,curloutput] = hawkcURLer(sshline);
  [aquilaposition,falcoposition,status,aquilaerror,falcoerror] = parseHawk(curloutput);

  if ~status
    disp 'Bad position, couldn''t see one or both cameras.  moving to next point.'
  end
    
    capturedpoints(indexvar).aquila = aquilaposition;
    capturedpoints(indexvar).falco = falcoposition;
    %grab the pps position (yeah, it's offset by tooltip, shaddap.)
    capturedpoints(indexvar).ppsposition = ppsposition;
    ppsposition(7) = ppsposition(4);
    ppsposition(4) = [];
    capturedpoints(indexvar).correctedppsposition = ppsposition;
    capturedpoints(indexvar).positionerror = positionerror;
    capturedpoints(indexvar).aquilaerror = aquilaerror;
    capturedpoints(indexvar).falcoerror = falcoerror;
    capturedpoints(indexvar).status = status;   

  
  
  if indexvar == pointsrequired
    acquiring = false;
  end
  
end


fixedppspositions = removeZerosFromStruct(capturedpoints);




%%disp 'Calculating least squares for a changed aquila...'
%%correctwhichcamera = 'aquila'
%%
%%[aquilax, obj, info, iter, nf, lambda] = sqp([rt_to_isocentric_params(aquilacameraposition),rt_to_isocentric_params(falcocameraposition)],@phidouble,[],[],[],[],1000,1E-300)
%%disp 'least squares calculated, cool! old objective is below.'
%%[meritvalue,meritarray]=calcMeritFunction(capturedpoints)
%%
%%
%%disp 'new aquila vector:'
%%toCameraYAML(aquilax(1:6))
%%disp 'Calculating least squares for a changing both cameras'
%%correctwhichcamera = 'both';
%%
%%[bothx, obj, info, iter, nf, lambda] = sqp([rt_to_isocentric_params(aquilacameraposition),rt_to_isocentric_params(falcocameraposition)],@phidouble,[],[],[],[],1000,1E-300)
%%disp 'least squares calculated, objective improved from:'
%%[meritvalue,meritarray]=calcMeritFunction(capturedpoints)
%%
%%
%%disp 'new aquila only vector:'
%%toCameraYAML(aquilax(1:6))
%%disp 'new falco only vector'
%%toCameraYAML(falcox(7:12))
%%
%%disp 'new vectors for both'
%%disp 'aquila'
%%toCameraYAML(bothx(1:6))
%%disp 'falco'
%%toCameraYAML(bothx(7:12))
%%
%%
%%
%%
%%
