## HawkInit
#clear





#[aquilacameraposition,falcocameraposition]= getCameraPosition(sshline);


capturedpoints = removeZerosFromStruct(capturedpoints);
disp 'Calculating least squares for a changed falco...'

[falcox, obj, info, iter, nf, lambda] = sqp([rt_to_isocentric_params(falcocameraposition)],@phi,[],[],[],[],1000,1E-300)
disp 'least squares calculated, cool! old objective is below.'
[meritvalue,meritarray]=calcMeritFunction(capturedpoints)


disp 'new falco vector:'
toCameraYAML(falcox)
[a,aa] = calcMeritFunction(calcNewMeritFunction(capturedpoints, falcox))
disp 'sum points that were over 1mm'
sum(meritarray>1)
sum(aa>1)


##disp 'Calculating least squares for a changed aquila...'
##correctwhichcamera = 'aquila'
##
##[aquilax, obj, info, iter, nf, lambda] = sqp([rt_to_isocentric_params(aquilacameraposition),rt_to_isocentric_params(falcocameraposition)],@phidouble,[],[],[],[],1000,1E-300)
##disp 'least squares calculated, cool! old objective is below.'
##[meritvalue,meritarray]=calcMeritFunction(capturedpoints)
##
##
##disp 'new aquila vector:'
##toCameraYAML(aquilax(1:6))
##disp 'Calculating least squares for a changing both cameras'
##correctwhichcamera = 'both';
##
##[bothx, obj, info, iter, nf, lambda] = sqp([rt_to_isocentric_params(aquilacameraposition),rt_to_isocentric_params(falcocameraposition)],@phidouble,[],[],[],[],1000,1E-300)
##disp 'least squares calculated, objective improved from:'
##[meritvalue,meritarray]=calcMeritFunction(capturedpoints)
##
##
##disp 'new aquila only vector:'
##toCameraYAML(aquilax(1:6))
##disp 'new falco only vector'
##toCameraYAML(falcox(7:12))
##
##disp 'new vectors for both'
##disp 'aquila'
##toCameraYAML(bothx(1:6))
##disp 'falco'
##toCameraYAML(bothx(7:12))
##
##
##
##
##
