function [rootsumsquare] = calcRSS(aquilamatrix,falcomatrix)
  #handle incoming IEC vectors
  if length(aquilamatrix)==6
    temp = aquilamatrix;
    clear aquilamatrix;
    aquilamatrix = generate_IEC61217_Deq0(temp);
  endif
  if length(falcomatrix)==6
    temp = falcomatrix;
    clear falcomatrix;
    falcomatrix = generate_IEC61217_Deq0(temp);
  endif
   
  rootsumsquare = 1E3*sqrt((aquilamatrix(1,4)-falcomatrix(1,4))^2+(aquilamatrix(2,4)-falcomatrix(2,4))^2+ (aquilamatrix(3,4)-falcomatrix(3,4))^2);
  
  
endfunction
