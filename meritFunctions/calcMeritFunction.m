function [meritvalue,meritarray] = calcMeritFunction(points1)

  meritvalue = 0;
  
  # figure out if this is the initial calc or not
  
  nonewfalco = ~isfield(points1,'newfalco');
  nonewaquila = ~isfield(points1,'newaquila');
  # winding coefficient
  nexp  = 2.5;
  nlin = 1000;
  if nonewfalco && nonewaquila
    #do initial calc.
    for i=1:length(points1);
      meritarray(i) = calcRSS(points1(i).aquila,points1(i).falco);
      meritvalue = meritvalue + (nlin.^(meritarray(i)>.5))*meritarray(i)^nexp;      
    
    endfor
  elseif ~nonewfalco && nonewaquila
    #newfalco only calc.      
    for i=1:length(points1);
      meritarray(i) = calcRSS(points1(i).aquila,points1(i).newfalco);
      meritvalue = meritvalue + (nlin.^(meritarray(i)>.5))*meritarray(i)^nexp;            
    endfor
  elseif ~nonewaquila && nonewfalco
    #newaquila only calc.      
    for i=1:length(points1);
      meritarray(i) = calcRSS(points1(i).newaquila,points1(i).falco);
      meritvalue = meritvalue + (nlin.^(meritarray(i)>.5))*meritarray(i)^nexp;            
    endfor
 
  else
    #both new calc.   
    
    for i=1:length(points1);
      meritarray(i) = calcRSS(points1(i).newaquila,points1(i).newfalco);
      meritvalue = meritvalue + (nlin.^(meritarray(i)>.5))*meritarray(i)^nexp;      
    endfor
  end
  
  
endfunction
