function [rootsumsquare] = calcRSSNoAngles(aquilamatrix,falcomatrix)
  #handle incoming nonIEC vectors
  if length(aquilamatrix)~=6
    temp = aquilamatrix;
    clear aquilamatrix;
    aquilamatrix = rt_to_IEC61217_Deq0_params(temp);
  endif
  if length(falcomatrix)~=6
    temp = falcomatrix;
    clear falcomatrix;
    falcomatrix = rt_to_IEC61217_Deq0_params(temp);
  endif
   
  rootsumsquare = sqrt((aquilamatrix(1)-falcomatrix(1))^2+(aquilamatrix(2)-falcomatrix(2))^2+(aquilamatrix(3)-falcomatrix(3))^2);
  
  
endfunction
