function capturedpoints = calcNewMeritFunction(capturedpoints,x)
  
  
  fourbyx = generate_IEC61217_Deq0(x);
  
  falcocameraposition = evalin('base','falcocameraposition');

  for i = 1: length(capturedpoints)
    capturedpoints(i).newfalco = fourbyx * falcocameraposition^-1 * capturedpoints(i).falco;
    
  end
  [a,aaa] = calcMeritFunction(capturedpoints);
  
endfunction
