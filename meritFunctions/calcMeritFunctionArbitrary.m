function [meritvalue,meritarray] = calcMeritFunction(points1)

  meritvalue = 0;
  
  # alright which camera are we optimizing, here
  solverfield = evalin('base','solverfield');
  
  # winding coefficient
  nexp  = 0;
  nlin = 1;
  nbad = .5
  
    #do initial calc.
    for i=1:length(points1);
      meritarray(i) = calcRSSAngles(points1(i).(solverfield),points1(i).trackervalues;
      meritvalue = meritvalue + (nlin.^(meritarray(i)>nbad))*meritarray(i)^(nexp*(meritarray(i)>nbad)+1);      
    
    endfor
    
 
endfunction
