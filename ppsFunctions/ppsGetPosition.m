function [IECPosition,ppsstate] = ppsGetPosition(sshline)
  
  [status,output] = ppsGetStatus(sshline);
  IECPosition = loadjson(output).position;
  ppsstate = loadjson(output).state;
  
endfunction
