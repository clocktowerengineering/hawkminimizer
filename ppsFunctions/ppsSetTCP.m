function [status,output] = ppsSetTCP(sshline,IECVector)
    rz = IECVector(6);
    rx = IECVector(4);
    ry = IECVector(5);
    rotationmatrix = [rz,rx,ry];
    ppsline = [sshline,' curl -s -H \"Accept: application/json\" -H \"Content-Type: application/json\" -X POST http://192.168.104.161:8081/api --data ''{\""command\"":\""setTCP\"",\""origin\"":',mat2PPS(IECVector(1:3)),',\""rotation\"":',mat2PPS(rotationmatrix),'}'';'];

    [status,output] = system(ppsline);
    
endfunction
  
