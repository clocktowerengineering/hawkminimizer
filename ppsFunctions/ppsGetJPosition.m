function [jposition,ppsstate] = ppsGetJPosition(sshline)
  
  [status,output] = ppsGetStatus(sshline);
  jposition = loadjson(output).jPosition;
  ppsstate = loadjson(output).state;
  
endfunction
