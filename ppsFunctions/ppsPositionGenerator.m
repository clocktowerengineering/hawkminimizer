function [ppspositions] = ppsPositionGenerator(squaredistance=.40,squaresteps=3,heightdistance=.2,heightsteps=2,pitchrolljitter = false, angles=[0,pi/2,pi], initial = [0,0,0,0,0,0])
    # define 1 sigma pitchroll and initialized x and y
    pitch1sigma = deg2rad(.2);
    roll1sigma = deg2rad(.2);
    xangle = 0;
    yangle = 0;
    # reminder: anglegeneration is in radians!!!!
    # calculate step distances
    
    squarestepdistance = squaredistance/squaresteps;
    heightstepdistance = heightdistance/heightsteps;
    
    ppspositions = initial;
    ppspositionindex = 1;
    # alright, let's make some for loopage
    
    for angleindex = 1:length(angles)
      
      for zindex = 1:(heightsteps+1)
        
        #add flipx behavior to reduce time between stepping.  for each z height, the direction of x is flipped, so if this is done properly minimum distance moves should be enforced.
         if mod(zindex,2)
            flipx = -1;
          else
            flipx = 1;
          endif
        
        
        for xindex = 1:(squaresteps+1)
          
          #add flipy behavior to reduce time during stepping.  for each x row, the direction of y is flipped, so if this is done properly minimum distance moves should be enforced
          
          
          
          
          if mod(xindex,2)
            flipy = -1;
          else
            flipy = 1;
          endif
          
          
          for yindex = 1:(squaresteps+1)
    
            xval = -(flipx*squaredistance/2) + flipx*squarestepdistance*(xindex-1);
            yval = -(flipy*squaredistance/2) + flipy*squarestepdistance*(yindex-1);
            zval = -(heightdistance/2) + heightstepdistance*(zindex-1);
            zangle = angles(angleindex);
            if pitchrolljitter
              xangle = randn()*pitch1sigma;
              yangle = randn()*roll1sigma;
            endif
            
            ppspositions(ppspositionindex,:) = [ppspositions(1,1)+xval,ppspositions(1,2)+yval,ppspositions(1,3)+zval,ppspositions(1,4)+zangle,ppspositions(1,5)+xangle,ppspositions(1,6)+yangle];
            ppspositionindex = ppspositionindex + 1;
          endfor
          
        endfor
      endfor
    endfor
    
            
  
  
  
  
endfunction
