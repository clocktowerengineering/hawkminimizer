function [ppsposition,atposition,positionerror] = ppsToPositionBlocking(sshline, IECVector,handedness=91)
  # fundamentally the same as ppsToPosition but adds blocking and doesn't return until the PPS is in position.
  # ppsposition returns the position from the pps as IEC vector.
  # atposition returns true if successfully at position, false if not.  tries to get to position given 3x.
  # clear errors.
  
  #note: need to add initial joint levelling routine  
  [status,ppsstatus] = ppsGetStatus(sshline);
  
  # ok, check for handedness of robot
  
  robotelbow = loadjson(ppsstatus).elbow;
  
  # ok, and now check the handedness of the current IECVector
  
  if IECVector(4)<deg2rad(handedness)
    wantelbow = 'LEFT';
  else
    wantelbow = 'RIGHT';
  endif
  
  #ok, see if want and get are the same
  
  if ~strcmp(robotelbow,wantelbow)
    
    # wants to be left elbow.  Send to 0,0,0,0,0,0 on left.
    
    if strcmp(wantelbow,'LEFT')
      
      [status,output] = ppsToJoints(sshline,[-1.1702417135238647,1.0008199214935303,0.0004996528732590377,-0.006559401284903288,-0.00015207774990482785,0.12843558489725126]);
     
    else
      
      [status,output] = ppsToJoints(sshline,[1.1737544536590576,-0.9607361555099487,-0.04756966605782509,0.007924649864435196,0.05208441943929182,-0.24881115543032079]);
      
    endif
    pause(20)
  endif
  
  atposition = false;
  triescounter = 0;
  
  while (~atposition && triescounter<4)
    triescounter = triescounter + 1;
    # send the robot to a position.
    [status,output] = ppsToPosition(sshline,IECVector);
    
    moving = true;
    
    
    while moving
      pause(.25);
      #beep
      [ppsposition,ppsstatus] = ppsGetPosition(sshline);
      IECVector;
      ppsposition;
      
      if ~strcmp(ppsstatus,'BUSY')
        moving = false;
        pause(.1)
      endif
      
    endwhile
    
    # ok, we aren't moving.  check if we're at the actual position
    positionerror = calcRSS(ppsposition,IECVector);
    if positionerror< .5
      
      atposition=true;

    else 
    #something happened.  clear errors and try again.
    endif
    
  endwhile
  
  pause(.25)
    
    
 
endfunction