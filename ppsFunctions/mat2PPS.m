function output = mat2PPS(inputvector)
  
  output = ['[',sprintf('%.12f,' , inputvector)(1:end-1),']'];
  
endfunction
