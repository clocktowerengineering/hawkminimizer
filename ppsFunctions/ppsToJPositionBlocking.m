function [jposition,atposition,positionerror] = ppsToJPositionBlocking(sshline, jposition,handedness=91)
  # fundamentally the same as ppsToPosition but adds blocking and doesn't return until the PPS is in position.
  # ppsposition returns the position from the pps as IEC vector.
  # atposition returns true if successfully at position, false if not.  tries to get to position given 3x.
  # clear errors.
  
  
  
  atposition = false;
  triescounter = 0;
  
  while (~atposition && triescounter<4)
    triescounter = triescounter + 1;
    # send the robot to a position.
    [status,output] = ppsToJoints(sshline,jposition);
    
    moving = true;
    
    
    while moving
      pause(.25);
      #beep
      [ppsposition,ppsstatus] = ppsGetJPosition(sshline);

      
      if ~strcmp(ppsstatus,'BUSY')
        moving = false;
        pause(.1)
      endif
      
    endwhile
    
    # ok, we aren't moving.  check if we're at the actual position
    positionerror = calcRSS(ppsposition,jposition);
    if positionerror< .5
      
      atposition=true;

    else 
    #something happened.  clear errors and try again.
    endif
    
  endwhile
  
  
 pause(.5)   
    
 
endfunction