function [camerapoints] = newcameraposition(capturedpoints,newcameraposition)
  
  
  for i = 1: length(capturedpoints)
    
    camerapoints = newcameraposition * capturedpoints;
  endfor
  
  
endfunction
