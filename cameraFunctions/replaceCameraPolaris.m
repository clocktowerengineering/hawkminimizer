# camerareplacer

# pass this an aquila value and a falco value as an 12 axis IEC vector aquila first
# it'll go off, find the folder in hawkland, then blow away the line with "position" in it
# then replace it with the new position, because that's the right thing to do
# then when it's done, send the KILL EVERYTHING command to reset the hawkcURLer
# then pause 10 seconds
# then return

function [status,output] = replaceCameraPolaris(sshline, input)
  
  
  # grab all the texts
  disp('Replacing positions.')
  aquilacameraline = [sshline, ' ssh -i /home/mcs/id_rsa root@192.168.104.160 sed -i \''/position/c\',toCameraYAML(input(1:6)),'\'' /usr/lib/hawk-embedded/app/conf/polaris/aquila.yml'];
  
  [status,output] = system(aquilacameraline);
  
  falcocameraline = [sshline, ' ssh -i /home/mcs/id_rsa root@192.168.104.160 sed -i \''/position/c\',toCameraYAML(input(7:12)),'\'' /usr/lib/hawk-embedded/app/conf/polaris/falco.yml'];
  
  [status,output] = system(falcocameraline);
  
  resetcameraline = [sshline, ' ssh -i /home/mcs/id_rsa root@192.168.104.160 /home/hawk/restart.sh'];
  
  [status,output] = system(resetcameraline);
  
endfunction
