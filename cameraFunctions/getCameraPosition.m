function [aquilacameraposition,falcocameraposition] = getCameraPosition(sshline)
  
  # grab all the texts
  disp('Grabbing the files from HAWK')
  cameraline = [sshline, ' ssh -i /home/mcs/id_rsa root@192.168.104.160 cat /usr/lib/hawk-embedded/app/conf/polaris/*.*'];
  #'ssh root@192.168.102.100  ssh -i /home/mcs/id_rsa root@192.168.104.160 cat /usr/lib/hawk-embedded/app/conf/polaris/*.*');
  
  [status,output] = system(cameraline);
  startpos = strfind(output,'position:');
  endpos = strfind(output,'...');

  aquilacameraposition = str2num(strrep(strrep(strrep(output((startpos(1)+10):endpos(1)-3),'],','];'),']',''),'[',''));
  falcocameraposition = str2num(strrep(strrep(strrep(output((startpos(2)+10):endpos(2)-3),'],','];'),']',''),'[',''));
  

  
endfunction
