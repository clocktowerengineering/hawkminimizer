# feed this a vector of length 6 and it'll output a formatted camera position.  Fun.



function retval = toCameraYAML (x)


  values = generate_IEC61217_Deq0(x);
    
  outputstring = 'position: [';
  for i = 1:3
    outputstring = strcat(outputstring,'[');
    for j = 1:4
      
      outputstring = strcat(outputstring,sprintf('%.16f',values(i,j)));
      outputstring = strcat(outputstring,',');
    endfor
    outputstring = strcat(outputstring,'],');
  endfor
  
  outputstring = strcat(outputstring,'[0.0,0.0,0.0,1.0]]');
  
retval = outputstring;

endfunction
