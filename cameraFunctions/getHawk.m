function [aquilaposition,falcoposition,status,aquilaerror,falcoerror] = getHawk(sshline)
  
  [status,output] = hawkcURLer(sshline);
  [aquilaposition,falcoposition,status,aquilaerror,falcoerror] = parseHawk(output);
  
endfunction
