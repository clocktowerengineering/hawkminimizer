function [aquilaposition,falcoposition,status,aquilaerror,falcoerror] = parseHawk(inputline)
  aquilaposition = eye(4);
  falcoposition = eye(4);
  aquilaerror = 0.0;
  falcoerror = 0.0;
  status = true;
  # take the input and check for falco and aquilaposition
  jsonstring = loadjson(inputline);
  if ~isfield(jsonstring,'detailed_positions')
    # it can't see anything. return false
    status = false;
    
  else
      
    
    # alright, go check whether jsonstring has both variables
    jsonparsed = cell2mat(jsonstring.detailed_positions);
    
    # if not, status is false

    if length(jsonparsed)~=2
      
      status = false;
    endif
    
    # ok, regardless, let's go ahead and parse this
     
      #alright, parse this shit
    for i=1:length(jsonparsed)
      
      if strcmp(jsonparsed(1,i).sensor,'aquila');
        
        aquilaposition = jsonparsed(1,i).position;
        aquilaerror = jsonparsed(1,i).error;
      
      elseif strcmp(jsonparsed(1,i).sensor ,'falco');
        
        falcoposition = jsonparsed(1,i).position;
        falcoerror = jsonparsed(1,i).error;
      end
      
    endfor
    
  endif
  
  
  
    
  
endfunction
