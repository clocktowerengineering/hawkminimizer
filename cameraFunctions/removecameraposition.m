function [camerapoints] = removecameraposition(capturedpoints,cameraposition)
  
  length(capturedpoints)
  for i = 1: length(capturedpoints)
    
    camerapoints(i) = cameraposition^-1 * capturedpoints(i);
  endfor
endfunction
