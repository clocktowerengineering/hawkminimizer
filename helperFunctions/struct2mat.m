function matrixoutput = struct2mat(inputstruct,field)
  # takes an input structure with matrices at each index within the struct and pumps it out to a matrix that is length(inputstruct) rows and vector of the matrix columns.
  sizeofvector = prod(size(inputstruct(1).(field)));
  matrixoutput = reshape(inputstruct(1).(field),[1 sizeofvector]);
  for i=2:length(inputstruct)
    matrixoutput(i,:) = reshape(inputstruct(i).(field),[1 sizeofvector]);  
    
    
  endfor
endfunction
