function inputstruct = removeZerosFromStruct(inputstruct,whichcamera='aquila',cleanup=1, removezeroerror = 0 )
  index = 0;
  looping = true;
  cameraerrorval = [whichcamera,'error'];
  while looping
    index = index +1 ;
    # oh, also clean up the PPS positions while we are here
    if cleanup
      inputstruct(index).ppsposition(7) = inputstruct(index).ppsposition(4);
      inputstruct(index).ppsposition(4) = [];
    endif
    
    # if there's a request to remove items with zero error, do it here
    
    if removezeroerror
      
      if inputstruct(index).(cameraerrorval) < 0.1
        inputstruct(index)=[];
        index = index - 1;  
      endif
    endif
    
    if index~=0
      if isempty(inputstruct(index).(whichcamera)) && inputstruct(index).(cameraerrorval) >= 0.1
        inputstruct(index)=[];
        index = index - 1;
      endif
    endif
       
    
    if index == length(inputstruct)
      #hit the end, stop looping and return
      looping = false;
      
    endif
  endwhile
  
  

endfunction
