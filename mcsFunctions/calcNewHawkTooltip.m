function [new_hawk_tooltip] = calcNewHawkTooltip(cameraposition, ppsposition, orion_tooltip)
 
  
  new_hawk_tooltip = rt_to_IEC61217_Deq0_params((cameraposition^-1*generate_IEC61217_Deq0(ppsposition)*generate_IEC61217_Deq0(orion_tooltip)^-1)^-1);
  
  
endfunction
