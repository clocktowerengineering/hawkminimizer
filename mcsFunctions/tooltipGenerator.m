ppsSetTCP(sshline,[0,0,0,0,0,0]);

for i = 1:30
  temp1 = getHawk(sshline );
  temp2 = ppsGetPosition(sshline );
  temp2(7) = temp2(4);
  temp2(4) = [];
  value(i,:) = rt_to_IEC61217_Deq0_params(generate_IEC61217_Deq0(temp2)^-1*temp1);
end
mean(value);

disp( [mat2str(mean(value)(1)),',',mat2str(mean(value)(2)),',',mat2str(mean(value)(3)),',',mat2str(mean(value)(4)),',',mat2str(mean(value)(5)),',',mat2str(mean(value)(6)),])

[status,output] = ppsSetTCP(sshline,mean(value))