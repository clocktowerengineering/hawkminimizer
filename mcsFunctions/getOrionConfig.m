## Copyright (C) 2019 Mark
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see
## <https://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {} {@var{retval} =} getOrionConfig (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Mark <Mark@DESKTOP-OMVJIGR>
## Created: 2019-02-15

function [orion_tooltip,orion_isocenter] = getOrionConfig (sshline)

  [status, output ] = system([sshline," cat /etc/mcs/ppcs_app/orion_config.xml"]);
  
  tooltiplocations = strfind(output,"ORION_TOOLTIP");
  
  orion_tooltip = str2num(output(tooltiplocations(1)+15:tooltiplocations(2)-4));
  
  isocenterlocations = strfind(output,"ISOCENTER");
  
  orion_isocenter = str2num(output(isocenterlocations(1)+11:isocenterlocations(2)-4));

endfunction
