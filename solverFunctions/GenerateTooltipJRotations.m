# this script grabs the current value of the pps then rotates isocentrically in Z about the end effector, grabs the camera values, and presents them for plotting.  LET'S DO IT!


initppsjposition =[     -1.689670443535   0.789622306824  -0.035958111286   0.004100406542   0.032508965093   0.866532903698]
initialzanglereal = pi/2;
startangle = 0.866532903698 - pi/2;
angles = startangle:pi/90:(pi/2+startangle);
cameraselector = 'aquila';
generatedppspositions = initppsjposition;

for i = 1: length(angles)
  
  generatedppspositions(i,:) = initppsjposition;
  generatedppspositions(i,6) = angles(i);
  
endfor


successfulpoints = 0;

for indexvar = 1:length(generatedppspositions)
    disp(['going to jposition ',mat2str(indexvar),'/',mat2str(length(generatedppspositions)),'. Position: ',mat2str(generatedppspositions(indexvar,:)), '. Successful points so far:',mat2str(successfulpoints)]);
    [ppsposition,atposition,positionerror] = ppsToJPositionBlocking(sshline,generatedppspositions(indexvar,:),rad2deg((startangle+pi/2))+1);
    
    #move succeeded.  
    if atposition
      
      #check the camerastatus.
      pause(1)
      [status,curloutput] = hawkcURLer(sshline);
      [aquilaposition,falcoposition,status,aquilaerror,falcoerror] = parseHawk(curloutput);
      
      if ~status
        disp 'Bad position, couldn''t see one or both cameras.  moving to next point.'
        capturedpoints(indexvar).aquila = aquilaposition;
        capturedpoints(indexvar).falco = falcoposition;
        #grab the pps position (yeah, it's offset by tooltip, shaddap.)
        capturedpoints(indexvar).ppsposition = ppsposition;
        capturedpoints(indexvar).positionerror = positionerror;
        capturedpoints(indexvar).status = status;        
        
     
     else
        capturedpoints(indexvar).aquila = aquilaposition;
        capturedpoints(indexvar).falco = falcoposition;
        #grab the pps position (yeah, it's offset by tooltip, shaddap.)
        capturedpoints(indexvar).ppsposition = ppsposition;
        capturedpoints(indexvar).positionerror = positionerror;
        successfulpoints = successfulpoints + 1;
        capturedpoints(indexvar).status = status;
      endif
    
    else
      #move failed. go to next point.
      disp 'Move index failed.  going to next point.'
    endif
  endfor
  

# reorder and remove zeros  
fixedppspositions = removeZerosFromStruct(capturedpoints )
for i = 1: length(fixedppspositions )
  fixedppspositions(i).aquilaIEC = rt_to_IEC61217_Deq0_params (fixedppspositions (i).aquila);
  fixedppspositions(i).falcoIEC = rt_to_IEC61217_Deq0_params (fixedppspositions (i).falco);
  fixedppspositions(i).camerarss = calcRSS(fixedppspositions(i).aquila,fixedppspositions(i).falco);
  fixedppspositions(i).aquilappsrss = calcRSS(fixedppspositions(i).aquila,fixedppspositions(i).ppsposition);
  fixedppspositions(i).falcoppsrss = calcRSS(fixedppspositions(i).falco,fixedppspositions(i).ppsposition)  ;
end



initialguess = fixedppspositions(1).(cameraselector);
 [x2, obj, info, iter, nf, lambda] = sqp(initialguess(1:3),@findCenterPoint,[],[],[],[10,10,0],1000,1E-300)


% find per unit error

  for i = 1: length(fixedppspositions)
    IECVector = fixedppspositions(i).([cameraselector,'IEC']);
    xyzdistances2(i) = sqrt((IECVector(1)-x2(1))^2+(IECVector(2)-x2(2))^2+(IECVector(3)-x2(3))^2);
     
  endfor
 
 
max(xyzdistances2)
mean(xyzdistances2)
min(xyzdistances2)

#  great!  we've found the center of rotation for the given curve in xyz space.  how far away is this from the place we thought it should be?



