function obj = fitCameraToPPSPhi(x)
  
  
  
  # here's the phi function for optimization.
  # alright which camera are we optimizing, here
  cameraselector = evalin('base','cameraselector');
  # and what's the second value we're optimizing, here
  secondoptimizer = evalin('base','secondoptimizer');
  structname = evalin('base','structname');
  # create new string for new cameravalues
  newcamerastring = ['new',cameraselector];
  
  # aaaand let's grab the struct we're optimizing
  
  capturedpoints = evalin('base',structname);
  
  # take the input x vector and check that it's a length 6 vector.  
  
  if length(x) ~=6
    disp 'BAD INPUT!!!'
     
  end
  
  #ok, it's a good vector.  convert vector to IEC.
  
  fourbyx = generate_IEC61217_Deq0(x);
  
  # grab globals.
  
  # okay, calculate new positions.
  for i = 1: length(capturedpoints)
    capturedpoints(i).(newcamerastring) = (capturedpoints(i).(cameraselector)^-1 * fourbyx )^-1;
    
  end

  [meritvalue,meritarray]=calcCameraToPPSMerit(capturedpoints,newcamerastring,secondoptimizer);
  obj = meritvalue;
  
endfunction
