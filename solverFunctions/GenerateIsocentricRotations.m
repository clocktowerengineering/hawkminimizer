# this script grabs the current value of the pps then rotates isocentrically in Z about that point, grabs the camera values, and presents them for plotting.  LET'S DO IT!

# todo: fix the plotting and also fix cases where cameraerror is 0 (ie the camera can't see, dudemar)
clear fixedppspositions
clear capturedpoints
clear vals

#camerabasisshift = [0,0,0,0,0,0]
#close all
replaceCameraPolaris(sshline,[rt_to_IEC61217_Deq0_params(generate_IEC61217_Deq0(camerabasisshift)*generate_IEC61217_Deq0(solvedaquilafalco(1:6))),rt_to_IEC61217_Deq0_params(generate_IEC61217_Deq0(camerabasisshift)*generate_IEC61217_Deq0(solvedaquilafalco(7:12)))]);

howfar = pi/2;

disp 'press SHIFT+A to take the current position as the 0 position.'
disp 'press SHIFT+T to take the current position as the 90 position.'
disp 'press SHIFT+I to use some bullshit preplanned point.'
disp 'any other keys will use existing positionry.'
disp 'if no key is pressed, preplanned.'
pause(5)

newpoints = kbhit(1);

if newpoints == 'A'
  initppsposition = ppsGetPosition(sshline)
  startangle = initppsposition(4);
  
elseif newpoints == 'T'
  initppsposition = ppsGetPosition(sshline)
  startangle = initppsposition(4)-pi/2;

elseif newpoints == 'I' || isempty(newpoints)
  initppsposition =[ -0.0004215796537  -0.0117368784593  -0.1101922346353  -2.6681720057152   0.0094718647641  -3.1356145448187]
  startangle = initppsposition(4)-pi/2;
endif


angles = startangle:pi/45:(howfar+startangle);
cameraselector = 'aquila';
generatedppspositions = initppsposition;

for i = 1: length(angles)
  
  generatedppspositions(i,:) = initppsposition;
  generatedppspositions(i,4) = angles(i);
  
endfor


successfulpoints = 0;

for indexvar = 1:length(generatedppspositions)
    disp(['going to position ',mat2str(indexvar),'/',mat2str(length(generatedppspositions)),'. Position: ',mat2str(generatedppspositions(indexvar,:)), '. Successful points so far:',mat2str(successfulpoints)]);
    [ppsposition,atposition,positionerror] = ppsToPositionBlocking(sshline,generatedppspositions(indexvar,:),rad2deg((startangle+pi/2))+1);
    
    #move succeeded.  
    if atposition
      
      #check the camerastatus.
      pause(1)
      [status,curloutput] = hawkcURLer(sshline);
      [aquilaposition,falcoposition,status,aquilaerror,falcoerror] = parseHawk(curloutput);
      
      if ~status
        disp 'Bad position, couldn''t see one or both cameras.  moving to next point.'
        capturedpoints(indexvar).aquila = aquilaposition;
        capturedpoints(indexvar).falco = falcoposition;
        #grab the pps position (yeah, it's offset by tooltip, shaddap.)
        capturedpoints(indexvar).ppsposition = ppsposition;
        ppsposition(7) = ppsposition(4);
        ppsposition(4) = [];
        capturedpoints(indexvar).correctedppsposition = ppsposition;
        capturedpoints(indexvar).positionerror = positionerror;
        capturedpoints(indexvar).aquilaerror = aquilaerror;
        capturedpoints(indexvar).falcoerror = falcoerror;
        capturedpoints(indexvar).status = status;        
        
     
     else
        capturedpoints(indexvar).aquila = aquilaposition;
        capturedpoints(indexvar).falco = falcoposition;
        #grab the pps position (yeah, it's offset by tooltip, shaddap.)
        capturedpoints(indexvar).ppsposition = ppsposition;
        ppsposition(7) = ppsposition(4);
        ppsposition(4) = [];
        capturedpoints(indexvar).correctedppsposition = ppsposition;
        capturedpoints(indexvar).positionerror = positionerror;
        capturedpoints(indexvar).aquilaerror = aquilaerror;
        capturedpoints(indexvar).falcoerror = falcoerror;
        capturedpoints(indexvar).status = status; 
        successfulpoints = successfulpoints + 1;
      endif
    
    else
      #move failed. go to next point.
      disp 'Move index failed.  going to next point.'
    endif
  endfor
  

# reorder and remove zeros  
fixedppspositions = removeZerosFromStruct(capturedpoints, whichcamera = cameraselector, cleanup=0, removezeroerror=1);
fixedppspositions = removeZerosFromStruct(fixedppspositions, whichcamera = 'falco', cleanup=0, removezeroerror=1);

for i = 1: length(fixedppspositions )
  fixedppspositions(i).aquilaIEC = rt_to_IEC61217_Deq0_params (fixedppspositions (i).aquila);
  fixedppspositions(i).falcoIEC = rt_to_IEC61217_Deq0_params (fixedppspositions (i).falco);
  fixedppspositions(i).camerarss = calcRSS(fixedppspositions(i).aquila,fixedppspositions(i).falco);
  fixedppspositions(i).aquilappsrss = calcRSS(fixedppspositions(i).aquila,fixedppspositions(i).correctedppsposition);
  fixedppspositions(i).falcoppsrss = calcRSS(fixedppspositions(i).falco,fixedppspositions(i).correctedppsposition)  ;
end

# TODO: remove any bad statuses


figure('Name',datestr(now))
# plot runout as XY per camera
plot(1000*(struct2mat(fixedppspositions,'aquilaIEC')(:,1)-mean(struct2mat(fixedppspositions,'aquilaIEC')(:,1))),1000*(struct2mat(fixedppspositions,'aquilaIEC')(:,2)-mean(struct2mat(fixedppspositions,'aquilaIEC')(:,2))));
hold on
plot(1000*(struct2mat(fixedppspositions,'falcoIEC')(:,1)-mean(struct2mat(fixedppspositions,'falcoIEC')(:,1))),1000*(struct2mat(fixedppspositions,'falcoIEC')(:,2)-mean(struct2mat(fixedppspositions,'falcoIEC')(:,2))));
hold off
axis equal
xlabel('x pos, mm')
ylabel('y pos, mm')
legend('aquila position','falco position')

figure('Name',datestr(now))
plot([0:rad2deg(howfar)/(length(fixedppspositions)-1):rad2deg(howfar)],struct2mat(fixedppspositions,'falcoppsrss'),[0:rad2deg(howfar)/(length(fixedppspositions)-1):rad2deg(howfar)],struct2mat(fixedppspositions,'aquilappsrss'));
xlabel('angle');
ylabel('rss error in mm');
legend('falco','aquila');

for i = 1:length(fixedppspositions)
  temp = fixedppspositions(i).aquilaIEC;
  vals(i,1) = temp(1);
  vals(i,2) = temp(2);
  vals(i,3) = temp(3);
endfor


figure('Name',[datestr(now)," ",mat2str(camerabasisshift)])
subplot(3,1,1)
plot([0:rad2deg(howfar)/(length(fixedppspositions)-1):rad2deg(howfar)],vals(:,1))
legend('x val')
xlabel('rz angle')
ylabel('value in m')
subplot(3,1,2)

plot([0:rad2deg(howfar)/(length(fixedppspositions)-1):rad2deg(howfar)],vals(:,2))
legend('y val')
xlabel('rz angle')
ylabel('value in m')
subplot(3,1,3)

plot([0:rad2deg(howfar)/(length(fixedppspositions)-1):rad2deg(howfar)],vals(:,3))
legend('z val')
xlabel('rz angle')
ylabel('value in m')
