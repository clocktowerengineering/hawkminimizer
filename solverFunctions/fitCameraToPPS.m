

acquiring = false;

disp 'press SHIFT+A to automatically go through a preplanned point cloud.'
disp 'any other keys will use existing point cloud.'
pause(2)
newpoints = kbhit(1);

if newpoints == 'A'
  # call the pps autocapture function.
  
  #generate ppscloud.
  
  generatedppspositions = ppsPositionGenerator(squaredistance=.15,squaresteps=6,heightdistance=.03,heightsteps=2,pitchrolljitter = false, angles=[0,-pi/16, -pi/8,-pi/6,-pi/4], initial = currentposition);
  successfulpoints = 0;
  for indexvar = 1:length(generatedppspositions)
    disp(['going to position ',mat2str(indexvar),'/',mat2str(length(generatedppspositions)),'. Successful points so far:',mat2str(successfulpoints)]);
    [ppsposition,atposition,positionerror] = ppsToPositionBlocking(sshline,generatedppspositions(indexvar,:));
    
    #move succeeded.  
    if atposition
      
      #check the camerastatus.
      
      [aquilaposition,falcoposition,status,aquilaerror,falcoerror] = getHawk(sshline);
      
      if ~status
        disp 'Bad position, couldn''t see one or both cameras.  moving to next point.'
        capturedpoints(indexvar).aquila = aquilaposition;
        capturedpoints(indexvar).falco = falcoposition;
        #grab the pps position (yeah, it's offset by tooltip, shaddap.)
        capturedpoints(indexvar).ppsposition = ppsposition;
        ppsposition(7) = ppsposition(4);
        ppsposition(4) = [];
        capturedpoints(indexvar).correctedppsposition = ppsposition;
        capturedpoints(indexvar).positionerror = positionerror;
        capturedpoints(indexvar).aquilaerror = aquilaerror;
        capturedpoints(indexvar).falcoerror = falcoerror;
        capturedpoints(indexvar).status = status;        
        
     
     else
        capturedpoints(indexvar).aquila = aquilaposition;
        capturedpoints(indexvar).falco = falcoposition;
        #grab the pps position (yeah, it's offset by tooltip, shaddap.)
        capturedpoints(indexvar).ppsposition = ppsposition;
        ppsposition(7) = ppsposition(4);
        ppsposition(4) = [];
        capturedpoints(indexvar).correctedppsposition = ppsposition;
        capturedpoints(indexvar).positionerror = positionerror;
        capturedpoints(indexvar).aquilaerror = aquilaerror;
        capturedpoints(indexvar).falcoerror = falcoerror;
        capturedpoints(indexvar).status = status; 
        successfulpoints = successfulpoints + 1;
      endif
    
    else
      #move failed. go to next point.
      disp 'Move index failed.  going to next point.'
    endif
  endfor

endif

  
  # alright, let's generate the fixedppspoints from the pov of aquila.
  
  fixedppspoints = removeZerosFromStruct(capturedpoints,'aquila',cleanup=0);
  # ok now falco
  
  fixedppspoints = removeZerosFromStruct(fixedppspoints,'falco',cleanup=0);
  
  # generate raw camera position for aquila now.
  
  for i = 1: length(fixedppspoints)
    fixedppspoints(i).aquilaRaw = aquilacameraposition^-1 * fixedppspoints(i).aquila;
    fixedppspoints(i).falcoRaw = falcocameraposition^-1 * fixedppspoints(i).falco;
  endfor
  
  # alright, let's grind pps vectors now.
  # assuming the above PPS position has end effector of EE = identity:
  # ppsposition[n] = cameraPosition*aquilaRaw*HawkTCP^-1.  
  # aquila and falco each are cameraPositionOld*rawNDIcameraposition . 
  # cameraPosition and HawkTCP are unknowns.  12 vars to solve here.
  solverfield = 'aquilaRaw';
  solverfield2 = 'falcoRaw';
  solverstruct = 'fixedppspoints';
  
  [solvedaquila, obj, info, iter, nf, lambda] = sqp([rt_to_IEC61217_Deq0_params(aquilacameraposition),hawk_tooltip],@cameraToPPSSolver,[],[],[-10,-10,-10,-2*pi,-2*pi,-2*pi,-10,-10,-10,-2*pi,-2*pi,-2*pi],[10,10,10,2*pi,2*pi,2*pi,10,10,10,2*pi,2*pi,2*pi],500,1E-300)
  solvedaquila = transpose(solvedaquila);
  newaquilacameraposition = generate_IEC61217_Deq0(solvedaquila(1:6));



  
##  for i = 1: length(fixedppspoints)
##    temp3(i).newaquilaposition = generate_IEC61217_Deq0(solvedaquila(1:6))*aquilacameraposition^-1*fixedppspoints(i).aquila;
##    temp3(i).newfalcoposition = generate_IEC61217_Deq0(solvedaquila(7:12))*falcocameraposition^-1*fixedppspoints(i).falco;
##  end
  
  
  
  
 
  
  
  
  
  
  