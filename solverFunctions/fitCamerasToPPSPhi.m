
clc
[orion_tooltip,orion_isocenter] = getOrionConfig (sshline);
[hawk_tooltip] = getHawkConfig (sshline);

[aquilacameraposition,falcocameraposition]= getCameraPosition(sshline);
correctwhichcamera = 'aquila';
currentposition = ppsGetPosition(sshline);

acquiring = false;

disp 'press SHIFT+A to automatically go through a preplanned point cloud.'
disp 'any other keys will use existing point cloud.'
newpoints = kbhit();

if newpoints == 'A'
  # call the pps autocapture function.
  
  #generate ppscloud.
  
  generatedppspositions = ppsPositionGenerator(squaredistance=.10,squaresteps=4,heightdistance=.03,heightsteps=1,pitchrolljitter = false, angles=[0,-pi/16], initial = currentposition);
  successfulpoints = 0;
  for indexvar = 1:length(generatedppspositions)
    disp(['going to position ',mat2str(indexvar),'/',mat2str(length(generatedppspositions)),'. Successful points so far:',mat2str(successfulpoints)]);
    [ppsposition,atposition,positionerror] = ppsToPositionBlocking(sshline,generatedppspositions(indexvar,:));
    
    #move succeeded.  
    if atposition
      
      #check the camerastatus.
      
      [aquilaposition,falcoposition,status,aquilaerror,falcoerror] = getHawk(sshline);
      
      if ~status
        disp 'Bad position, couldn''t see one or both cameras.  moving to next point.'
        capturedpoints(indexvar).aquila = aquilaposition;
        capturedpoints(indexvar).falco = falcoposition;
        #grab the pps position (yeah, it's offset by tooltip, shaddap.)
        capturedpoints(indexvar).ppsposition = ppsposition;
        ppsposition(7) = ppsposition(4);
        ppsposition(4) = [];
        capturedpoints(indexvar).correctedppsposition = ppsposition;
        capturedpoints(indexvar).positionerror = positionerror;
        capturedpoints(indexvar).aquilaerror = aquilaerror;
        capturedpoints(indexvar).falcoerror = falcoerror;
        capturedpoints(indexvar).status = status;        
        
     
     else
        capturedpoints(indexvar).aquila = aquilaposition;
        capturedpoints(indexvar).falco = falcoposition;
        #grab the pps position (yeah, it's offset by tooltip, shaddap.)
        capturedpoints(indexvar).ppsposition = ppsposition;
        ppsposition(7) = ppsposition(4);
        ppsposition(4) = [];
        capturedpoints(indexvar).correctedppsposition = ppsposition;
        capturedpoints(indexvar).positionerror = positionerror;
        capturedpoints(indexvar).aquilaerror = aquilaerror;
        capturedpoints(indexvar).falcoerror = falcoerror;
        capturedpoints(indexvar).status = status; 
        successfulpoints = successfulpoints + 1;
      endif
    
    else
      #move failed. go to next point.
      disp 'Move index failed.  going to next point.'
    endif
  endfor

endif

  
  # alright, let's generate the fixedppspoints from the pov of aquila.
  
  fixedppspoints = removeZerosFromStruct(capturedpoints,'aquila',cleanup=0);
  # ok now falco
  
  fixedppspoints = removeZerosFromStruct(fixedppspoints,'falco',cleanup=0);
  
  # generate raw camera position for aquila now.
  
  for i = 1: length(fixedppspoints)
    fixedppspoints(i).aquilaRaw = aquilacameraposition^-1 * fixedppspoints(i).aquila;
    fixedppspoints(i).falcoRaw = falcocameraposition^-1 * fixedppspoints(i).falco;
  endfor
  
  # alright, let's grind pps vectors now.
  # assuming the above PPS position has end effector of EE = identity:
  # ppsposition[n] = cameraPosition*aquilaRaw*HawkTCP^-1.  
  # aquila and falco each are cameraPositionOld*rawNDIcameraposition . 
  # cameraPosition and HawkTCP are unknowns.  12 vars to solve here.
  solverfield = 'aquilaRaw';
  solverfield2 = 'falcoRaw';
  solverstruct = 'fixedppspoints';
  
  [solvedaquilafalco, obj, info, iter, nf, lambda] = sqp([rt_to_IEC61217_Deq0_params(aquilacameraposition),rt_to_IEC61217_Deq0_params(falcocameraposition),0,0,0,0,0,0],@camerasToPPSSolver,[],[],[],[],500,1E-300)
  solvedaquilafalco = transpose(solvedaquilafalco);
  newaquilacameraposition = generate_IEC61217_Deq0(solvedaquilafalco(1:6));
  newfalcocameraposition = generate_IEC61217_Deq0(solvedaquilafalco(7:12));
  HawkTCP = solvedaquilafalco(13:18);
  
  disp 'aquilapos.'
  toCameraYAML (solvedaquilafalco(1:6))
  disp 'falcopos.'
  toCameraYAML(solvedaquilafalco(7:12))
  
  replaceCameraPolaris(sshline,solvedaquilafalco);
  
##  for i = 1: length(fixedppspoints)
##    temp3(i).newaquilaposition = generate_IEC61217_Deq0(solvedaquilafalco(1:6))*aquilacameraposition^-1*fixedppspoints(i).aquila;
##    temp3(i).newfalcoposition = generate_IEC61217_Deq0(solvedaquilafalco(7:12))*falcocameraposition^-1*fixedppspoints(i).falco;
##  end
  
  
  
  
 
  
  
  
  
  
  