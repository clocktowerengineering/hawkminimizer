function obj = camerasToPPSSolver(x)
  
  # this function takes a single 1x18 vector, splits them into three 1x6 vectors,
  # treats them as IEC vectors, then computes the new position given by the cameras
  # and compares that to the PPS using RSS.  It returns the RSS value between 
  # the PPS positions given and these new camera positions as calculated.
  
  if length(x) ~=18
    disp 'BAD INPUT!!!AAAAAAAAAAAAAAAAAAAAAA'
     
  end
  
  cameraposition1 = generate_IEC61217_Deq0(x(1:6));
  cameraposition2 = generate_IEC61217_Deq0(x(7:12));
  oldcameraposition1 = evalin('base','aquilacameraposition');
  oldcameraposition2 = evalin('base','falcocameraposition');
  HawkTCP = generate_IEC61217_Deq0(x(13:18));
  
  obj = 0;
  # alright which camera are we optimizing, here
  solverfield = evalin('base','solverfield');
  solverfield2 = evalin('base','solverfield2');
  
  # and what's the name of the struct?
  structname = evalin('base','solverstruct');
  # create new string for new cameravalues
  
  # aaaand let's grab the struct we're optimizing
  
  fixedppspositions = evalin('base',structname);
  
  # generate new positions for each position, calculate RSS, and sum.
  for i=1:length(fixedppspositions)
    
    fixedppspositions(i).newvalue = cameraposition1*fixedppspositions(i).(solverfield)*HawkTCP^-1;
    fixedppspositions(i).newvalue2 = cameraposition2*fixedppspositions(i).(solverfield2)*HawkTCP^-1;
    
    obj = obj + calcRSSAngles(fixedppspositions(i).newvalue,generate_IEC61217_Deq0(fixedppspositions(i).correctedppsposition));
    obj = obj + calcRSSAngles(fixedppspositions(i).newvalue2,generate_IEC61217_Deq0(fixedppspositions(i).correctedppsposition));
  endfor
  
  
  
endfunction
