require 'optparse'
require 'rconfig'

$project_path = File.expand_path "../..", __FILE__
$root_path    = File.expand_path "../../lib", __FILE__
$:.push $root_path

require 'hawk/server'
require 'hawk/configuration'

Hawk::Config.load
Hawk::Server.run!
