require 'hawk/polaris_driver_usb'
require 'hawk/polaris_driver_eth'
require 'hawk/position_handler'
require 'delegate'
require 'matrix'

# This version of Polaris.rb adds the following features:
# 
# 1) Configurable schottky behavior for bad and good: if there are enough missing samples, the system stays in missing until there are enough good samples.
# 2) Configurable averaging behavior: each camera does a windowing average around the last windowed number of good samples on the quaternion on a per-term basis (7 value vector).


module Hawk
  class Polaris < Delegator
    include PositionHandler

    attr_reader :baudrate, :position, :driver, :port_handle, :tool

    alias :__getobj__ :driver

    def initialize(device, baudrate, position, target_file)
      @driver = ((/(\d{1,3}\.){3}\d{1,3}/ =~ device) ? (PolarisDriverTCP.new device) : (PolarisDriverSerial.new device))
      @position = position
      @baudrate = baudrate
      @badstate = true
      @badcounter = 0
      @badtrigger = 5
      @goodtrigger = 30
      @averagepositions = 10
      @targetvalues = Array.new(8) {Array.new(@averagepositions)}
      @goodcounter = @goodtrigger
      @targetaverage = Array.new(8)
      @tool   = File.open(target_file).read
    end

    def init
      driver.reset
      driver.comm_settings baudrate if driver.class == PolarisDriverSerial
      driver.init
      driver.set("Param.Tracking.Selected Volume=1")
      driver.set("Param.Tracking.Sensitivity=1")

      @port_handle = driver.assign_ph

      driver.load_tool_file port_handle, tool
      driver.init_ph port_handle
      driver.enable_ph port_handle
    end

    # Read polaris information and return the target position
    # in the Fixed Reference System as a 4x4 matrix in SI
    def target_position
      target = driver.get_tool_trans[port_handle].first

      if (target.first == 'MISSING')

        @badcounter = @badcounter + 1

        if @badcounter>@badtrigger
          @badcounter = @badtrigger
          @goodcounter = 0
          @badstate = true
        end
#        return 'MISSING'
      else
        @goodcounter = @goodcounter + 1
        if @goodcounter > @goodtrigger
          @goodcounter = @goodtrigger
          @badcounter = 0
          @badstate = false
        end

      end

      if @badstate == false && target.first!='MISSING'
  
        # routine time: we want to average the last @averagepositions positions and use that as the quaternion value instead of the raw last quaternion.
        #
        # method:
        # 
        # 1) append the newest value of target[n] to @targetvalues[n].
        # 2) check whether the length of @targetaverage[n] is greater than @averagepositions.
        # 3) if it is, remove the oldest value from @targetvalues[n].
        # 4) take the average of @targetvalues[n] for each [n] and store that average value to targetaverage[n].
        
        # 1) push the newest value of target[n] to @targetvalues[n]. 


        for n in 0..7
          @targetvalues[n].push(target[n])
        
        # 2) check whether the length of @targetaverage[n] is greater than @averagepositions.
        
         
          if @targetvalues[n].size > @averagepositions
            # 3) if it is, remove the oldest value from @targetvalues[n]. 
          
            # to do this, for each n in @targetvalues[n], shift the array by one.
            @targetvalues[n].shift

                 
          end
          
          # 4) take the average of @targetvalues[n] for each [n] and store that average value to @targetaverage[n].
          @targetaverage[n] = @targetvalues[n].compact.reduce(:+) / @targetvalues[n].compact.size.to_f
        end


        qTaPo = @targetaverage[0..3] + @targetaverage[4..6].map {|val| val / 1000.0}
        mTaPo = quaternionToMatrix Vector.elements(qTaPo)
        @singlevalid = true

        {'position' => Matrix[*position] * mTaPo,
         'error' => @targetaverage[7]}

      # okay, what if we missed a couple cycles?  use the existing targetaverage.
      elsif @badstate == false
        @singlevalid = false

        qTaPo = @targetaverage[0..3] + @targetaverage[4..6].map {|val| val / 1000.0}
        mTaPo = quaternionToMatrix Vector.elements(qTaPo)
        @singlevalid = true

        {'position' => Matrix[*position] * mTaPo,
         'error' => @targetaverage[7]}

      else
        return 'MISSING'
      end
    end

    # Return the operating mode.
    # Possible result are "Setup", "Reset", "Tracking" and "Diagnostic".
    def mode
      driver.get("Info.Status.System Mode")
    end

  end
end
