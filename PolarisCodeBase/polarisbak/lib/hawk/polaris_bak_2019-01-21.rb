require 'hawk/polaris_driver_usb'
require 'hawk/polaris_driver_eth'
require 'hawk/position_handler'
require 'delegate'
require 'matrix'

module Hawk
  class Polaris < Delegator
    include PositionHandler

    attr_reader :baudrate, :position, :driver, :port_handle, :tool

    alias :__getobj__ :driver

    def initialize(device, baudrate, position, target_file)
      @driver = ((/(\d{1,3}\.){3}\d{1,3}/ =~ device) ? (PolarisDriverTCP.new device) : (PolarisDriverSerial.new device))

      @position = position
      @baudrate = baudrate
      @tool   = File.open(target_file).read
    end

    def init
      driver.reset
      driver.comm_settings baudrate if driver.class == PolarisDriverSerial
      driver.init
      driver.set("Param.Tracking.Selected Volume=1")
      driver.set("Param.Tracking.Sensitivity=1")

      @port_handle = driver.assign_ph

      driver.load_tool_file port_handle, tool
      driver.init_ph port_handle
      driver.enable_ph port_handle
    end

    # Read polaris information and return the target position
    # in the Fixed Reference System as a 4x4 matrix in SI
    def target_position
      target = driver.get_tool_trans[port_handle].first

      if target.first == 'MISSING'
        return 'MISSING'
      else
        qTaPo = target[0..3] + target[4..6].map {|val| val / 1000.0}
        mTaPo = quaternionToMatrix Vector.elements(qTaPo)

        {'position' => Matrix[*position] * mTaPo,
         'error' => target[7]}
      end
    end

    # Return the operating mode.
    # Possible result are "Setup", "Reset", "Tracking" and "Diagnostic".
    def mode
      driver.get("Info.Status.System Mode")
    end

  end
end
