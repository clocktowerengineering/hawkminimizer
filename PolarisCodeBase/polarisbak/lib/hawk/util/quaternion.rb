module Hawk

  class Quaternion
    include Enumerable

    ANGLE_TOLERANCE=0.00001
    attr_accessor :q0,:q1,:q2,:q3

    def initialize(q0=1.0,q1=0.0,q2=0.0,q3=0.0)
      @q0,@q1,@q2,@q3 = q0,q1,q2,q3
    end

    def to_s
      "(%0.4f + %0.4fi + %0.4fj + %0.4fk)" % [@q0, @q1, @q2, @q3]
    end

    def +(q)
      case q
      when Quaternion
        Quaternion.new(@q0+q.q0, @q1+q.q1, @q2+q.q2, @q3+q.q3)
      when Numeric
        raise "Addition with a scalar is not implemented"
      end
    end

    def -(q)
      Quaternion.new(@q0-q.q0, @q1-q.q1, @q2-q.q2, @q3-q.q3)
    end

    def *(q)
      case q
      when Quaternion
        n_q0 = -@q1 * q.q1 - @q2 * q.q2 - @q3 * q.q3 + @q0 * q.q0;
        n_q1 =  @q1 * q.q0 + @q2 * q.q3 - @q3 * q.q2 + @q0 * q.q1;
        n_q2 = -@q1 * q.q3 + @q2 * q.q0 + @q3 * q.q1 + @q0 * q.q2;
        n_q3 =  @q1 * q.q2 - @q2 * q.q1 + @q3 * q.q0 + @q0 * q.q3;

        Quaternion.new(n_q0, n_q1, n_q2, n_q3)
      when Numeric
        Quaternion.new(@q0*q, @q1*q, @q2*q, @q3*q)
      else
       raise "Multiplication with an #{q.class} is not implemented"
      end
    end

    def coerce(other)
      [self, other]
    end

    def -@
      -1*self
    end

    def ==(q)
      @q0 == q.q0 && @q1 == q.q1 && @q2 == q.q2 && @q3 == q.q3
    end

    def norm
      Math.sqrt(square_norm)
    end

    def square_norm
      @q0*@q0 + @q1*@q1 + @q2*@q2 + @q3*@q3
    end

    def normalize
      n = self.norm
      Quaternion.new(@q0/n, @q1/n, @q2/n ,@q3/n)
    end

    def null?
      @q0 == 0 && @q1 == 0 && @q2 == 0 && @q3 == 0
    end

    def each(&block)
      [@q0, @q1, @q2, @q3].each &block
    end

    def close_to?(q, tol)
      (self - q).all? {|el| el.abs < tol}
    end

    def dot(q)
      @q0*q.q0 + @q1*q.q1 + @q2*q.q2 + @q3*q.q3
    end

    def inverse
      if null?
        raise "Impossible to inverse a null quaternion"
      else
        self.conj / (norm*norm)
      end
    end

    def conj
      Quaternion.new(@q0, -@q1, -@q2, -@q3)
    end

    def / (q) # /
      case q
      when Quaternion then self*q.inverse
      when Numeric then Quaternion.new(@q0/q, @q1/q, @q2/q, @q3/q)
      end
    end

    #Spherical Linear interpolation
    #return normalize quaternion
    def slerp(q, t = 0.5)
      a = self.normalize
      b = q.normalize

      raise "bad parameter value, t must be in ]0;1[" unless t>0 && t<1
      return b if a == b

      dot = a.dot(b)
      if dot < 0.0
        b = -b
        dot = -dot
      end

      theta = Math.acos(dot)
      if theta < ANGLE_TOLERANCE
        result = a + (b-a)*t
      else
        s_theta    = Math.sin(theta)
        s_t_theta  = Math.sin(t*theta / s_theta)
        s_1t_theta = Math.sin((1-t)*theta / s_theta)
        result = a*s_1t_theta + b*s_t_theta
      end

      result.normalize
    end

  end
end
