require 'sinatra/base'
require 'slim'
require 'json'

module Hawk
  module App

    class Status < Sinatra::Base

      attr_reader :core

      def initialize(core)
        @core = core

        super()
      end

      get '/' do
        #Retourne le status de l'application ainsi que celui de chacun des polaris
        content_type :json
        return JSON.generate(core.status)
      end
    end

  end
end
