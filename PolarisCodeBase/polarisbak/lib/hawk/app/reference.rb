require 'sinatra/base'
require 'slim'
require 'json'
require 'matrix'

module Hawk
  module App

    class Reference < Sinatra::Base

      attr_reader :core

      def initialize(core)
        @core = core

        super()
      end

      delete '/:reference_name' do
        # Supprime la référence "reference_name"

        begin
        core.delete params[:reference_name]
        rescue StandardError => err
          p "Error::#{err.class} : #{err.message}"
          err.class == "Missing" ? (return 404) : (return 403)
        end
        return 200
      end

      get '/' do
        # Retourne la liste de toute les références

        refs = core.references_list
        content_type :json
        return JSON.generate(refs)
      end

      get '/:reference_name' do
        # Retourne la reference "reference_name"

        begin
          content_type :json
          ref = core.get_reference params[:reference_name]
          ref_infos = {"name" => ref.name, "position" => ref.position.to_a}
          return JSON.generate(ref_infos)
        rescue StandardError => err
          p "Error::#{err.class} : #{err.message}"
          err.class == "Missing" ? (return 404) : (return 403)
        end
      end

      patch '/:reference_name' do
        # Modifie la reference "reference_name" avec la "position" donnée

        begin
          reference = JSON.parse request.body.read
          core.set_position(params[:reference_name], Matrix[*reference["position"]])
        rescue StandardError => err
          p "Error::#{err.class} : #{err.message}"
          err.class == "Missing" ? (return 404) : (return 403)
        end
        return 200
      end

      post '/:reference_name' do
        # Récupère la position courante de la cible et modifie la référence
        # "reference_name" ou la crée

        if params[:reference_name] == "Room"
          p "Room should not be modified"
          return 403
        end
        res = core.save_position_as_ref params[:reference_name]
        if core.references.references.any?{|ref| ref.name==params[:reference_name]}
          p "save #{params[:reference_name]}"
          201
        else
          p "fail to save #{params[:reference_name]}"
          500
        end
      end

      put '/:reference_name' do
        # Crée la référence "reference_name" avec la "position" donnée.

        begin
          reference = request.body.read
          reference = JSON.parse reference
          core.add(params[:reference_name],reference["position"])
        rescue StandardError => err
          p "Error::#{err.class} : #{err.message}"
          return 403
        end
        return 201
      end
    end

  end
end
