require 'sinatra/base'
require 'slim'
require 'json'

module Hawk
  module App

    class SelectReference < Sinatra::Base

      attr_reader :core

      def initialize(core)
        @core = core

        super()
      end

      post '/:reference_name' do
        #Sélectionne la référence "reference_name"
        begin
        core.select_reference params[:reference_name]
        rescue StandardError => err
          p "Error::#{err.class} : #{err.message}"
          err.class == Missing ? (return 404) : (return 400)
        end
        return 200
      end
    end

  end
end
