require 'sinatra/base'
require 'slim'
require 'json'
require 'extendmatrix'

module Hawk
  module App

    class Tracking < Sinatra::Base
      attr_reader :core

      def initialize(core)
        @core = core

        super()
      end

      get '/' do
        #Retourne la position de la cible dans la référence courante.
        answer = {'reference' => core.current_ref.name}
        positions = core.tracking
        content_type :json
        if positions.empty?
          answer['status'] = 'TARGET_MISSING'
        else
          minimal_error_position = positions.min{|a,b| a['error'] <=> b['error']}['position']
          answer['position'] = minimal_error_position.to_a
          positions.each{ |pol_ans| pol_ans['position'] = pol_ans['position'].to_a }
          answer['detailed_positions'] = positions
          answer['status'] = 'SUCCESSFUL'
        end
        JSON.generate answer
      end

      post '/' do
        #Retourne la position de la cible dans la référence "reference"
      end
    end

  end
end
