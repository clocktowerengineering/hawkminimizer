require 'sinatra/base'
require 'slim'
require 'json'

module Hawk
  module App

    class Initialize < Sinatra::Base

      attr_reader :core

      def initialize(core)
        @core = core

        super()
      end

      get '/' do
        setup_status = core.setup_status
        content_type :json
        return JSON.generate({status: setup_status[0], error: setup_status[1]})
      end

      post '/' do
        core.setup
        202
      end
    end

  end
end
