require 'sinatra/base'
require 'slim'
require 'json'

module Hawk
  module App

    class Config < Sinatra::Base
      attr_reader :core

      def initialize(core)
        @core = core

        super()
      end

      get '/all' do
        content_type :json

        RConfig.main.to_json

#         [RConfig.main, RConfig.aquila, RConfig.falco].to_json
#         RConfig.get_config_files('main').to_s
#         sortie = RConfig.cache_hash.to_s
#         sortie += "\n" + "\n" + RConfig.cache_files.to_s
#         sortie += "\n" + "\n" + RConfig.cache_config_files.to_s
#         sortie += "\n" + "\n" + RConfig.main.to_s
#         sortie += "\n" + "\n" + RConfig.aquila.to_s
#         sortie += "\n" + "\n" + RConfig.falco.to_s

      end

      get '/:parameter_name' do
        content_type :json

        {params['parameter_name'] => RConfig.main[params['parameter_name']]}.to_json
      end

    end
  end
end
