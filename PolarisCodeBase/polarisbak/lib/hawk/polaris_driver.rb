﻿require 'stringio'
require 'socket'
require 'mathn'

module Hawk

#   @abstract Read infos from a polaris sensor
#   @example
#   polaris = Polaris.new "/dev/ttyUSB0"
#   p polaris.reset
#   p polaris.comm_settings 115_200
#   p polaris.beep 2
#   p polaris.init
#   ph = polaris.assign_ph
#   
#   tool = File.open("room_calibration/8700449.rom").read
#   
#   p polaris.load_tool_file ph, tool
#   #p polaris.stop_tracking
#   p polaris.init_ph ph
#   p polaris.enable_ph ph
#   p polaris.start_tracking true
#   p polaris.get_tool_trans
#   
#   while true
#     tools = polaris.get_tool_trans_fast 1
#     t449 = tools.values.first.first
#   
#     puts "%2.3f %2.3f %2.3f\n" %  t449[4, 3] unless t449[4,3].nil?
#   
#   end
  class PolarisDriver
    PolarisStringError = {
      0x00 => "Success",
      0x01 => "Invalid command",
      0x02 => "Command too long",
      0x03 => "Command too short",
      0x04 => "Invalid CRC calculated for command",
      0x05 => "Time-out on command execution",
      0x06 => "Unable to set up new communication parameters",
      0x07 => "Incorrect number of parameters",
      0x08 => "Invalid port handle selected",
      0x09 => "Invalid mode selected",
      0x0a => "Invalid LED selected",
      0x0b => "Invalid LED state selected",
      0x0c => "Command is invalid while in the current mode",
      0x0d => "No tool is assigned to the selected port handle",
      0x0e => "Selected port handle not initialized",
      0x0f => "Selected port handle not enable",
      0x10 => "System not initialized",
      0x11 => "Unable to stop tracking",
      0x12 => "Unable to start tracking",
      0x13 => "Unable to read the tool definition file",
      0x14 => "Invalid position sensor characterization parameters",
      0x15 => "Unable to initialize the system",
      0x16 => "Unable to start Diagnostic mode",
      0x17 => "Unable to stop Diagnostic mode",
      0x18 => "Unable to check for environmental infrared light",
      0x19 => "Unable to read device's firmware version information",
      0x1a => "Internal system error",
      0x1b => "Unable to initialize for environmental infrared diagnostics",
      0x1c => "Unable to set marker activation signature",
      0x1d => "Unable to search for SROM device IDs",
      0x1e => "Unable to read SROM device data",
      0x1f => "Unable to write SROM device data",
      0x20 => "Unable to select SROM device for given port handle and SROM device ID",
      0x21 => "Unbale to test electrical current on tool",
      0x22 => "Enable tools are not supported by selected volume parameters",
      0x23 => "Commande parameter is out of range",
      0x24 => "Unable to select parameters by volume",
      0x25 => "Unable to determine the system's supported features list",
      0x28 => "Too many tools are enable",
      0x29 => "Main processor firmware is corrupt",
      0x2a => "No memory is available for dynamic allocation (heap is full)",
      0x2b => "The requested port handle has not been allocated",
      0x2c => "The request port handle has become unoccupied",
      0x2d => "All handles have been allocated",
      0x2e => "Incompatible firmware version",
      0x2f => "Invalid port description",
      0x30 => "Requested port is already assigned a port handle",
      0x31 => "Invalid input or output state",
      0x32 => "Invalid operation for the device associated with the specificed port handle",
      0x33 => "Feature not available",
      0x34 => "Parameter does not exist",
      0x35 => "Invalid value type",
      0x36 => "Parameter value set is out of valid range",
      0x37 => "Parameter array index is incorrect",
      0x38 => "Parameter size is incorrect",
      0x39 => "Permission denied",
      0x3b => "File not found",
      0x3c => "Error writing to file",
      0x40 => "Tool definition file error",
      0x41 => "Tool characteristics not supported",
      0xa2 => "General purpose input/output access on external SYNC port failed",
      0xf1 => "Too much environmental infrared light",
      0xf4 => "Unable to erase Flash SROM device",
      0xf5 => "Unable to write Flash SROM device",
      0xf6 => "Unable to read Flash SROM device",
      0xfe => "Connection timeout",
      0xff => "Unknown error"
    }

    class PolarisError < StandardError
      attr_reader :error
      def initialize(error)
        @error = error
      end

      def to_s
        PolarisStringError[@error]
      end
    end

    def initialize(*)
      raise StandardError.new("PolarisDriver could not be initialized, should initialize subclass instead.")
    end

    # Initialize the system.
    def init
      pl_write "INIT \r"
      check_answer pl_read
    end

    # Sounds the system beeper
    # 'count' is number of beep valid values are 1 to 9.
    def beep(count = 1)
      pl_write "BEEP %i\r" % count
      check_answer pl_read
    end


    # Initialize a port handle
    def init_ph(port_handle)
      pl_write "PINIT %02X\r" % port_handle
      check_answer pl_read
    end

    # Enable the reporting of transformation for a particular port handle.
    def enable_ph(port_handle, priority = 'D')
      pl_write "PENA %02X%c\r" % [port_handle, priority]
      check_answer pl_read
    end

    # Disable the reporting of transformation for a particular port handle.
    def disable_ph(port_handle)
      pl_write "PDIS %02X\r" % port_handle
      check_answer pl_read
    end

    # Start tracking mode
    def start_tracking(reset = false)
      if reset == false
        pl_write "TSTART \r"
      else
        pl_write "TSTART 80\r"
      end
      check_answer pl_read
    end

    # Stop tracking mode
    def stop_tracking
      pl_write "TSTOP \r"
      check_answer pl_read
    end

    # Returns the number of assigned port handles and the port status for each
    # one. Assigns a port handle to a wired tool, GPIO device, or strober.
    #
    # Valid options are :
    #
    # - '0' Reports all allocated port handles (default)
    # - '1' Reports port handles that need to be freed
    # - '2' Reports port handles thar are occupied, but not initialized or enabled
    # - '3' Reports port handles that are occupied and initialized, but not enabled
    # - '4' Reports enabled port handles
    #
    # Returns an hash which each keys are a port handle and each corresponding
    # values are a OR'd status with this map :
    #
    # bit 0 : Occupied
    # bit 1 : Switch 1 closed
    # bit 2 : Switch 2 closed
    # bit 3 : Switch 3 closed
    # bit 4 : Initialized
    # bit 5 : Enabled
    # bit 6 : Reserved
    # bit 7 : Tool detected from current sensing
    # bit 8-11 : Reserved

    def get_ph_status(option = "0")
      pl_write "PHSR 0%c\r" % option
      input = pl_read

      check_answer(input)

      input = StringIO.new input

      phs = Hash.new

      #<number of port handle>
      #<first port handle><first status>
      #...
      #<nth port handle><nth status>
      input.read(2).to_i(16).times do
        phs[input.read(2).to_i 16] = input.read(3).to_i 16
      end

      return phs
    end

    # Release system resources from an unused port handle.
    def release_ph(port_handle)
      pl_write "PHF %02X\r" % port_handle
      check_answer pl_read
    end

    # Assigns a port handle to a tool or GPIO device.
    def assign_ph
      pl_write "PHRQ *********1****\r"

      input = pl_read
      check_answer input

      # port handle
      input[0,2].to_i 16
    end

    # Assigns a tool definition file to a wireless tool, or overrides the SROM
    # device in a wired tool or GPIO.
    #
    # 'data' is the string representation of the file
    def load_tool_file(port_handle, data)
      # Align to size of 64 bytes
      data +=  "\0" * (64 - data.length % 64)

      binary = data.unpack "C*"
      offset = 0

      while offset < data.length
        send = "PVWR %02X%04X" % [port_handle, offset]
        binary[offset, 64].each { |w| send += "%02X" % w }
        send += "\r"
        pl_write send

        check_answer pl_read

        offset += 64
      end
      return 0
    end

    # Sets user parameter values.
    def set(user_parameter)
      pl_write "SET %s\r" % user_parameter
      check_answer pl_read
    end

    # Gets user parameter
    def get(user_parameter)
      pl_write "GET %s\r" % user_parameter

      answer = pl_read
      check_answer answer

      return answer.split('=').last[0..-5]
    end

    # Returns the latest tool transformation, individual marker positions, and
    # system status in text format.
    #
    # Valid options are :
    #
    # - 0x0001 Transformation data (default)
    # - 0x0002 Tool and marker information
    # - 0x0004 3d position of a single stray active -- Not implemented
    # - 0x0008 3d positions of markers on tools
    # - 0x0800 Transformation not normally reported
    # - 0X1000 3d positions of stray passive markers -- Not implemented
    #
    # This options can be OR'd
    #
    # Returns on Hash. Each keys are a port handle id and the corresponding value
    # is an array that contains the results of different options. Status is
    # reported has hey ':status'.
    #
    # Option 0x0001 result
    #  [ q0, qx, qy, qz, tx, ty, tz, error, status, frame ]
    # or if target missing
    #  [ "MISSING", status, frame ]
    #
    # Option 0x0002 result
    #  [ tool_information, marker_information ]
    #
    # Option 0x0008
    #  [ number_of_markers, out_of_volume], [tx1, ty1, tz1], ..., [txn, tyn, tzn]]

    def get_tool_trans(option = 1)
      option &= ~(0x0004 | 0x1000) # remove unimplemented option
      pl_write "TX %04X\r" % option

      input = pl_read
      check_answer input

      out = Hash.new
      input = StringIO.new input
      input.read(2).to_i(16).times do

        ph = input.read(2).to_i(16)
        out[ph] = Array.new

        # Option 0x0001 Transformation Data
        if option & 0x0001 == 0x0001
          # Test missing
          first = input.read(6)
          unless first =~ /MISSIN/ || first =~ /DISABL/ # MISSING || DISABLED
            q0 = first.to_i / 10000.0
            qx = input.read(6).to_i / 10000.0
            qy = input.read(6).to_i / 10000.0
            qz = input.read(6).to_i / 10000.0

            tx = input.read(7).to_i / 100.0
            ty = input.read(7).to_i / 100.0
            tz = input.read(7).to_i / 100.0

            error = input.read(6).to_i / 10000.0
          end

          unless first =~ /DISABL/
            # Read the complete string MISSING ('G') TODO simplify this section
            input.read 1 if first =~ /MISSIN/

            status = input.read(8).to_i 16
            frame  = input.read(8).to_i 16
          else
            # Read the complete string DISABLE ('LE') TODO simplify this section
            input.read 2
          end

          if first =~ /MISSIN/
            out[ph] << ["MISSING", status, frame]
          elsif first =~ /DISABL/
            out[ph] << ["DISABLED"]
          else
            out[ph] << [q0, qx, qy, qz, tx, ty, tz, error, status, frame]
          end
        end

        # Option 0x0002 Tool marker information
        if option & 0x0002 == 0x0002
          tool_info = input.read(2).to_i 16
          marker_info = input.read(20).split(//).map{ |c| c.to_i}
          out[ph] << [tool_info, marker_info]
        end

        # Option 0x0008 3d positions of markers on tools
        if option & 0x0008 == 0x0008
          number_of_markers = input.read(2).to_i(16)
          out_of_volume = input.read((number_of_markers / 4.0).ceil).to_i

          out[ph] << [number_of_markers, out_of_volume]
          number_of_markers.times do
            tx = input.read(7).to_i / 10000.0
            ty = input.read(7).to_i / 10000.0
            tz = input.read(7).to_i / 10000.0

            out[ph].last << [tx, ty, tz]
          end
        end

        # Read the carriage return separator
        input.read 1
      end

      out[:status] = input.read(4).to_i(16)

      return out
    end

    # Same has get_tool_trans but use binary transfer mode instead of text
    def get_tool_trans_fast(option = 1)
      pl_write "BX %04X\r" % option

      start, len, header_crc = pl_read(6).unpack("SSS")
      input = [start, len, header_crc].pack("SSS") + pl_read(len)
      check_answer input, pl_read(2).unpack("S").first

      out = Hash.new
      input = StringIO.new input
      input.read(6) # skip header
      input.read(1).unpack("C").first.times do

        ph = input.read(1).unpack("C").first
        ph_status = input.read(1).unpack("C").first
        out[ph] = Array.new

        # Option 0x0001 Transformation Data
        if option & 0x0001 == 0x0001
          # Test missing
          if ph_status == 0x01

            q0 = input.read(4).unpack("e").first
            qx = input.read(4).unpack("e").first
            qy = input.read(4).unpack("e").first
            qz = input.read(4).unpack("e").first

            tx = input.read(4).unpack("e").first
            ty = input.read(4).unpack("e").first
            tz = input.read(4).unpack("e").first

            error = input.read(4).unpack("e").first
          end
        end

        unless (ph_status & 0x04) == 0x04
          status = input.read(4).unpack("S").first
          frame  = input.read(4).unpack("S").first
        end

        # TODO Change output
        if (ph_status & 0x02) == 0x02
          out[ph] << ["MISSING", status, frame]
        elsif (ph_status & 0x04 ) == 0x04
          out[ph] << ["DISABLED"]
        else
          out[ph] << [q0, qx, qy, qz, tx, ty, tz, error, status, frame]
        end

        # Option 0x0002 Tool marker information
        if option & 0x0002 == 0x0002
          tool_info = input.read(1).unpack("C").first
          marker_info = Array.new
          input.read(10).unpack("C10").each{ |m| marker_info << ((m & 0xF0) >> 4); marker_info << (m & 0x0F);}
          out[ph] << [tool_info, marker_info]
        end

        # Option 0x0008 3d positions of markers on tools
        if option & 0x0008 == 0x0008
          number_of_markers = input.read(1).unpack("C").first
          out_of_volume = input.read((number_of_markers / 8.0).ceil).unpack("C")

          out[ph] << [number_of_markers, out_of_volume]
          number_of_markers.times do
            tx = input.read(4).unpack("e").first
            ty = input.read(4).unpack("e").first
            tz = input.read(4).unpack("e").first

            out[ph].last << [tx, ty, tz]
          end
        end
      end

      out[:status] = input.read(2).unpack("S").first
      return out
    end

    private
    def pl_read(len = nil)
      raise ArgumentError, "len must be an integer" unless len.is_a?(Integer) || len.is_a?(NilClass)
      data = String.new
      if len.nil?
        data = @pl.gets("\r")
      else
        data = @pl.read len
      end

      #p data
      #p data.each_byte.to_a.map{ |b| "0x%02X" % b}
      data
    end

    def pl_write(data)
      #p data
      @pl.write data
    end

    def check_answer(input, crc = nil)
      input.chomp!

      # Error code
      raise PolarisError.new(input[5,2].to_i(16)) if input =~ /ERROR/

      # Wrong CRC
      raise PolarisError.new(0x04) if check_crc(input, crc) == false

      # Sucess
      return 0x00
    end

    def check_crc(input, crc)
      if crc.nil?
        crc(input[0..-5]) == input[-4..-1].to_i(16)
      else
        crc(input) == crc
      end
    end

    # The crc16 calculation code is taken from the NDI guide (section example of
    # code) and from MiTk.
    def crc(input)
      oddparity = [0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0]
      crc16 = 0
      input.each_byte do |data|
        data = (data ^ (crc16 & 0xff)) & 0xff
        crc16 >>= 8
        crc16 ^= 0xc001 unless oddparity[data & 0x0f] ^ oddparity[data >> 4] == 0
        data <<= 6
        crc16 ^= data
        data <<= 1
        crc16 ^= data
      end
      return crc16
    end
  end

  # Return a rotation matrix (distance in meters) from quaternion
  def quaternion_to_matrix(q)
    a, b, c, d = q[0], q[1], q[2], q[3]
    x, y, z = q[4], q[5], q[6]
    aa = a*a
    bb = b*b
    cc = c*c

    Matrix[
      [aa + bb - cc - dd,   2*b*c - 2*a*d,     2*a*c + 2*b*d, x / 1000],
      [    2*a*d + 2*b*c, aa - bb + cc-dd,     2*c*d - 2*a*b, y / 1000],
      [    2*b*d - 2*a*c,   2*a*b + 2*c*d, aa - bb - cc + dd, z / 1000],
      [                0,               0,                 0,        1]
    ]
  end

end