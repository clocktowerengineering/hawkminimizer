require 'hawk/polaris'
require 'hawk/polaris_driver'
require 'hawk/configuration'
require 'hawk/position_handler'

module Hawk

  class PolarisHandler
    include PositionHandler

    attr_reader :trackers

    def initialize(trackers_conf, default_target)

      @trackers = {}

      trackers_conf.each do |name, conf|
        polaris = Polaris.new(
          conf['device'],
          conf['baudrate'],
          conf['position'],
          default_target
        )

        trackers[name] = polaris
      end
    end

    def initialize_polaris
      trackers.each do |name, polaris|
        polaris.init
        polaris.start_tracking true
      end
    end

    def target_position
      trackers.each.map do |name, polaris|
        full_position = polaris.target_position
        full_position['sensor'] = name if full_position.is_a? Hash
        full_position
      end
    end

    #Retourne les infos utiles aux commandes GET/ initialize.
    def setup_status
      status = "Tracking"
      status_list = []
      trackers.each do |name, tracker|
        status_list << tracker.mode
        status = status_list.last if status == "Tracking"
        status = "Error" if status != status_list.last
      end
      raise "Polaris states incoherance, operating mode are: #{status_list}" if status === "Error"
      status
    end

    #Retourne les infos utiles aux commandes GET/ status.
    def status
      trackers_status = {}
      trackers.each do |name, tracker|
        trackers_status[name] = {tracking: (tracker.mode === "Tracking"),
                                 target: (tracker.target_position['position'] === "MISSING" ? "MISSING" : "TRACKING"),
                                 error: ""}
      end
      trackers_status
    end

  end

end
