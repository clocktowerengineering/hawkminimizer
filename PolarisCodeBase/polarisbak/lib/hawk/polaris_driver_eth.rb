require 'hawk/polaris_driver'
require 'io/wait'

module Hawk

  class PolarisDriverTCP < PolarisDriver

    def initialize(ip, port=8765)
      @pl = TCPSocket.new ip, port
    end

    def reset
      @pl.wait 1
      return 0xff if start_tracking =~ /ERROR/
      sleep 1
    end
  end
end
