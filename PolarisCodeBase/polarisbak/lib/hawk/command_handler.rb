
module Hawk

  class GenericCommand
    def execute
    end

    def name
      self.class.name.tap{ |s| s[0] = s[0].downcase }
    end

  end

  class CommandHandler
    MethodsHash = {}

    def createCommandByName(name, *args)
      CommandHash[name].new *args
    rescue NoMethodError => e
      UnknownCommand.new
    end

    def createCommandClass
    end

  end

end