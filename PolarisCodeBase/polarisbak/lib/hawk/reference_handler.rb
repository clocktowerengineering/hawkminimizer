require 'matrix'

module Hawk
  class ReferenceHandler

    class Missing < StandardError; end
    class Existing < StandardError; end
    class Protected < StandardError; end
    class Selected < Protected; end

    #define the ReferenceHandler attributs
    attr_reader :references, :current

    def initialize
      @references = []

      #créer une reference par fichiers

    end

    def init(references)
      references.each do |name, ref|
        add(name, ref["position"].dup)
      end

      select 'Room'
    end

    # Selects the main reference by its name. Raise an error if doesn't exist.
    def select name
      @current = reference(name)
    end

    # Returns an object Reference specified by its name.
    def reference(name)
      raise Missing.new("#{name} reference doesn't exist") unless @references.any?{|ref| ref.name == name}
      @references.select{|ref| ref.name == name}.first
    end

    # Returns an object Reference specified by its name.
    def list
      @references.map{|ref| ref.name}
    end

    # Add reference object created with Reference.new to the reference list.
    def add(name, position)
      raise Existing.new("ref #{name} already exist") if @references.any?{|ref| ref.name == name}
      @references << Reference.new(name, position)
    end

    def delete(name)
      # doit surpprimer le fichier de la référence
      raise Selected.new("ref #{name} is currently selected, select an other one before delete this one") if name == @current.name
      raise Protected.new("ref #{name} is protected and could not be deleted") if name == "Room"
      @references.delete_if{|ref| ref.name == name}
    end

    def set_position(name, position)
      raise Missing.new("#{name} reference doesn't exist") unless @references.any?{|ref| ref.name == name}
      raise Selected.new("ref #{name} is currently selected, select an other one before modify this one") if name == @current.name
      raise Protected.new("ref #{name} is protected and could not be deleted") if name == "Room"
      @references.select{|ref| ref.name == name}.first.position = position
    end

    class Reference
      attr_accessor :position
      attr_accessor :name

      def initialize(name_or_hash, position=nil)
        case name_or_hash
        when Hash
          @position = Matrix[*name_or_hash[:position]]
          @name     = name_or_hash[:name]

          if @position.nil? or @name.nil? then raise ArgumentError.new("<position> field is missing") end
        when String, Symbol
          @position = Matrix[*position]
          @name = name_or_hash.to_s
        else
          raise TypeError
        end
      end
    end

  end
end