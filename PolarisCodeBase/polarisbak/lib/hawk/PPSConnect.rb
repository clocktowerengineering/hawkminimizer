require 'net/http'
require 'uri'
require 'json'

uri = URI.parse("http://192.168.104.161:8081/api")
request = Net::HTTP::Post.new(uri)
request.content_type = "application/json"
request["Accept"] = "application/json"
request.body = JSON.dump({
  "command" => "getStatus"
})

req_options = {
  use_ssl: uri.scheme == "https",
}

response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
  http.request(request)
end

puts JSON.parse(response.body)["state"]
