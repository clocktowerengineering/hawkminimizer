require 'hawk/polaris_driver_usb'
require 'hawk/polaris_driver_eth'
require 'hawk/position_handler'
require 'hawk/position_generator'
require 'delegate'
require 'matrix'
require 'json'
require 'net/http'
require 'uri'

# This version of Polaris.rb adds the following features:
#
# 1) Moving vs not moving check.  System checks whether the square of the root sum square (ie the sum square) of the difference between the current reported position and the value of the position @averagepositions ago is greater than @movingcriterionsquared.  If it is greater or the target is missing, the target is moving, so set @movingtarget to true and reset the @stationarycounter to 0.  If it is not greater, increment @stationarycounter, and if @stationarycounter is greater than @stationarycriteria, set @moving to false.  This functionality is enabled by setting @movingcheck to true.

# 2) target reporting behavior change for occlusions.  If @moving is true, report values the usual way: if tracking, return the target position, if not tracking, return MISSING, if error, return error.  If @moving is false, if the camera is tracking, pop the oldest value of target positions from the @targetvalues buffer and push the latest value, then report the average value of the last @averagepositions of @targetvalues buffer.  If camera is not tracking, report the average value of the last @averagepositions of @targetvalues buffer.  If error, report error.

# 3) flag @averageifmoving.  if @averageifmoving is true, while @moving, if tracking, return the average position over the last @averagepositions, otherwise, return the last position.

# 4) flag @movingcompareaverage.  if @movingcompareaverage is true, compare the present to the average position, if false, compare to the oldest value in the @targetvalues buffer.

# 5) PPS moving check.  System checks whether the PPS is presently in the READY state.  if it isn't, @movingtarget is treated as true.  the pps moving check can be enabled by setting @ppsmovingcheck to true.

# 6) PPS moving checkrate.  System checks every @ppsmovingcheckrate times that the tracking is called.  can be effectively disabled by setting @ppsmovingcheckrate = 1.

# 7) NOT WORKING BECAUSE WE NEED TO TRANSFORM BY THE INVERSE OF THE VALUE IN MCS. PPS mimic moving tracking response.  When @movingtarget and @ppsvalueswhenmoving, uses the last values from the pps position as the response.  Doesn't feed the @targetvalues buffer with pps values, still uses the hawk values if @averageifmoving.

# 8) Missing moving tracking response.  When @movingtarget and @missingwhenmoving, reports missing.  May/may not feed the buffer based on @averageifmoving.

# 9) Settle early.  If @settleearly is true, regardless of pps moving state, if @settlecriterionsquared is met, start feeding the buffer and treating it as if it's settled already.  As implemented will be treated as meeting the movingcriterionsquared check.


module Hawk
  class Polaris < Delegator
    include PositionHandler

    attr_reader :baudrate, :position, :driver, :port_handle, :tool

    alias :__getobj__ :driver

    def initialize(device, baudrate, position, target_file)
      @driver = ((/(\d{1,3}\.){3}\d{1,3}/ =~ device) ? (PolarisDriverTCP.new device) : (PolarisDriverSerial.new device))
      @position = position
      @baudrate = baudrate
      @tool   = File.open(target_file).read
      @averagepositions = 10
      # averaging init
      @targetvalues = Array.new(8) {Array.new(@averagepositions)}
      @targetaverage = Array.new(8)
      
      # tweakable settings
      
      
      @movingcriterionsquared = 0.1001 #moving criteria, in mm
      @ppsmovingcheckrate = 1
      @ppsmovingcounter = @ppsmovingcheckrate - 1 
      @settlecriterionsquared = 10.0 #settle criterion, in mm
      @stationarycounter = 0
      @stationarycriteria = 10
      @uri = URI.parse("http://192.168.104.161:8081/api")
      
      # flag config
      @averageifmoving = true
      @missingwhenmoving = true
      @movingcompareaverage = true
      @movingcheck = true
      @movingtarget = @movingcheck
      @ppsvalueswhenmoving = false
      @ppsmovingcheck = true
      @settleearly = false
      
      
      
    end
    
    def init
      driver.reset
      driver.comm_settings baudrate if driver.class == PolarisDriverSerial
      driver.init
      driver.set("Param.Tracking.Selected Volume=1")
      driver.set("Param.Tracking.Sensitivity=1")

      @port_handle = driver.assign_ph

      driver.load_tool_file port_handle, tool
      driver.init_ph port_handle
      driver.enable_ph port_handle
    end

    # Read polaris information and return the target position
    # in the Fixed Reference System as a 4x4 matrix in SI
    def target_position
      target = driver.get_tool_trans[port_handle].first

	  
      
      

      # behaviors #1 and #4 implemented here.
    
      
      
      # begin movingcheck.

      if @movingcheck
      
        if (target.first == 'MISSING')
        
          # missing target, must be moving. reset the stationarycounter.
          @stationarycounter = 0
          @movingtarget = true
          
        else # ok, we have a target, let's do the moving check.
        
          # first, load the averager.
          
          for n in 0..7
            @targetvalues[n].push(target[n])
          
          # 2) check whether the length of @targetaverage[n] is greater than @averagepositions.
          
           
            if @targetvalues[n].size > @averagepositions
              # 3) if it is, remove the oldest value from @targetvalues[n]. 
            
              # to do this, for each n in @targetvalues[n], shift the array by one to remove the oldest sample.
              @targetvalues[n].shift

                   
            end
            
            # 4) take the average of @targetvalues[n] for each [n] and store that average value to @targetaverage[n].
            
            @targetaverage[n] = @targetvalues[n].compact.reduce(:+) / @targetvalues[n].compact.size.to_f
            
          end
            
          # do the math of the sum squared difference either against the oldest value or the average value.  note that we are just using the quaternion spatial values instead of converting into the correct reference frame because the sum square difference of 3D spatial values is the same regardless of what reference we are in.  saves some math and some processing time.
          
          if @movingcompareaverage
            
            
            # do the math against the average, not the oldest value.
            currenterror = (@targetaverage[4].to_f-target[4].to_f)**2 + (@targetaverage[5].to_f-target[5].to_f)**2 + (@targetaverage[6].to_f-target[6].to_f)**2
            #puts "If, averages: #{@targetaverage[4].to_f},#{@targetaverage[5].to_f},#{@targetaverage[6].to_f}"
            #puts "If, raw: #{target[4].to_f},#{target[5].to_f},#{target[6].to_f}"
            
            
          else
          
            # do the math against the oldest value, not the average.
            currenterror = (@targetvalues[0][4].to_f-target[4].to_f)**2 + (@targetvalues[0][5].to_f-target[5].to_f)**2 + (@targetvalues[0][6].to_f-target[6].to_f)**2
            #puts "else, averages: #{@targetaverage[4].to_f},#{@targetaverage[5].to_f},#{@targetaverage[6].to_f}"
            #puts "else, raw: #{target[4].to_f},#{target[5].to_f},#{target[6].to_f}"
            
          end
          puts "currenterror: #{currenterror}, movingstatus: #{@movingtarget}, stationarycounter: #{@stationarycounter}"
          # behaviors 4 and 9 implemented here:
          
          
          # compare the currenterror against @movingcriterionsquared and @settlecriterionsquared
          # error is more than the moving criterion and we aren't moving, but we are also not settling either
          
          if ((currenterror > @movingcriterionsquared ) and not ((currenterror < @settlecriterionsquared)) and @settleearly)
          
            
            puts "system is moving."
            @movingtarget = true
            @stationarycounter = 0
          
          else
          
            #system may not be moving!  increment the counter.
            
            @stationarycounter = @stationarycounter + 1
            
            # check whether we've been stationary for long enough
            
            if @stationarycounter > @stationarycriteria
            
              #cool, we're stationary!
              
              @stationarycounter = @stationarycriteria
              @movingtarget = false
              
              
            end
          
          end
        

        
        end
      
      end
      
    
      # behavior #2 implemented here.
    
      if @movingtarget
    
      # behavior #8 is implemented here.
      # target is moving OR @missingwhenmoving flag is thrown.
      
        if (target.first == 'MISSING') or @missingwhenmoving
          
          # target is missing.
          return 'MISSING'
          
          
        # behavior #7 implemented here.
        
        #if the @ppsvalueswhenmoving flag:
        
        elsif @ppsvalueswhenmoving
          #puts "PPSVALUES."
          {'position' =>      generate_IEC61217_Deq0(JSON.parse(response.body)["position"][0].to_f, JSON.parse(response.body)["position"][1].to_f, JSON.parse(response.body)["position"][2].to_f, JSON.parse(response.body)["position"][3].to_f, JSON.parse(response.body)["position"][4].to_f, JSON.parse(response.body)["position"][5].to_f),
          'error' => 0.0}        
        
        
        
        
        else 
        
        
        # target is not missing.  we have to report something back to the rest of the HAWK system.
      
        # routine time: we want to have the option of averaging the last @averagepositions positions and use that as the quaternion value instead of the raw last quaternion.
        
        # method:
        # 
        # 1) append the newest value of target[n] to @targetvalues[n].
        # 2) check whether the length of @targetaverage[n] is greater than @averagepositions.
        # 3) if it is, remove the oldest value from @targetvalues[n].
        # 4) take the average of @targetvalues[n] for each [n] and store that average value to targetaverage[n].
        
        # 1) push the newest value of target[n] to @targetvalues[n]. 


          for n in 0..7
            @targetvalues[n].push(target[n])
          
          # 2) check whether the length of @targetaverage[n] is greater than @averagepositions.
          
           
            if @targetvalues[n].size > @averagepositions
              # 3) if it is, remove the oldest value from @targetvalues[n]. 
            
              # to do this, for each n in @targetvalues[n], shift the array by one to remove the oldest sample.
              @targetvalues[n].shift

                   
            end
            
            # 4) take the average of @targetvalues[n] for each [n] and store that average value to @targetaverage[n].
            
            @targetaverage[n] = @targetvalues[n].compact.reduce(:+) / @targetvalues[n].compact.size.to_f
            
          end
        
        
          # behavior #3 implemented here: ok, now either report the present value or the averaged value.
        
          if @averageifmoving
          
            qTaPo = @targetaverage[0..3] + @targetaverage[4..6].map {|val| val / 1000.0}
            mTaPo = quaternionToMatrix Vector.elements(qTaPo)
            {'position' => Matrix[*position] * mTaPo,
            'error' => @targetaverage[7]}

            
          else
          
            qTaPo = target[0..3] + target[4..6].map {|val| val / 1000.0}
            mTaPo = quaternionToMatrix Vector.elements(qTaPo)
            {'position' => Matrix[*position] * mTaPo,
            'error' => target[7]}
            
          end
        

        
        end
        
        
      # target is NOT moving!
      
      else
        
        if (target.first != 'MISSING')
        
          #target isn't really missing, it's occluded.  Check that the robot hasn't moved:
          
          # Behavior #5, #6 implemented here.
      
          @ppsmovingcounter = @ppsmovingcounter + 1
          
          if @ppsmovingcheck and (@ppsmovingcounter >= @ppsmovingcheckrate)
            @ppsmovingcounter = 0

            request = Net::HTTP::Post.new(@uri)
            request.content_type = "application/json"
            request["Accept"] = "application/json"
            request.body = JSON.dump({
              "command" => "getStatus"
            })

            req_options = {
              use_ssl: @uri.scheme == "https",
            }

            response = Net::HTTP.start(@uri.hostname, @uri.port, req_options) do |http|
              http.request(request)
            end

            if( JSON.parse(response.body)["state"] != "READY")
              #puts "pps state: #{JSON.parse(response.body)["state"]}, pps position: 
              #puts "#{JSON.parse(response.body)["position"][0]},#{JSON.parse(response.body)["position"][1},#{JSON.parse(response.body)["position"][2]},#{JSON.parse(response.body)["position"][3]},#{JSON.parse(response.body)["position"][4]},#{JSON.parse(response.body)["position"][5]}"
              @movingtarget = true
              @stationarycounter = 0
              return 'MISSING'
            end
           
            
          end
          
          # great, hasn't moved.
          
          #  update the average and report it.
        
          for n in 0..7
            @targetvalues[n].push(target[n])
          
          # 2) check whether the length of @targetaverage[n] is greater than @averagepositions.
          
           
            if @targetvalues[n].size > @averagepositions
              # 3) if it is, remove the oldest value from @targetvalues[n]. 
            
              # to do this, for each n in @targetvalues[n], shift the array by one to remove the oldest sample.
              @targetvalues[n].shift

                   
            end
            
            # 4) take the average of @targetvalues[n] for each [n] and store that average value to @targetaverage[n].
            
            @targetaverage[n] = @targetvalues[n].compact.reduce(:+) / @targetvalues[n].compact.size.to_f
            
          end
      

        
        end
        # puts 'Not MOVING!'
        # if the target is missing, who cares, just do the math on the present value set
        qTaPo = @targetaverage[0..3] + @targetaverage[4..6].map {|val| val / 1000.0}
        mTaPo = quaternionToMatrix Vector.elements(qTaPo)
        {'position' => Matrix[*position] * mTaPo,
        'error' => @targetaverage[7]}
      
	
      end
    
    end
    

    # Return the operating mode.
    # Possible result are "Setup", "Reset", "Tracking" and "Diagnostic".
    def mode
      driver.get("Info.Status.System Mode")
    end

  end
end
