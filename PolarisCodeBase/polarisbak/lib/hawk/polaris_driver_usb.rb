require 'hawk/polaris_driver'
require 'serialport'

module Hawk

  class PolarisDriverSerial <PolarisDriver

    def initialize(port)
      @pl = SerialPort.new port, 9600
      @pl.read_timeout = 0
    end

    def reset
      @pl.break 30
      @pl.set_modem_params({"baud" => 9600, "parity" => SerialPort::NONE, "data_bits" => 8, "stop_bits" => 1})
      return 0xff if pl_read =~ /ERROR/
      sleep 2
    end

    # TODO change parameter to match used by set_modem_params
    # Sets the serial communication settings for the system
    #
    # Valid parameters are :
    #
    # Baud rate :
    #   - 9600 (default)
    #   - 14_400
    #   - 19_200
    #   - 38_400
    #   - 57_600
    #   - 115_200
    #   - 921_600
    #   - 1_228_739
    #
    # Data bits :
    #   - 8 bits (default)
    #   - 7 bits
    #
    # Parity :
    #   - SerialPort::NONE (default)
    #   - SerialPort::ODD
    #   - SerialPort::EVEN
    #
    # Stop bits
    #
    #   - 1 stop bit (default)
    #   - 2 stop bit
    #
    # Hardware handshaking
    #   - 0 is off (default)
    #   - 1 is on -- Not implemented
    def comm_settings(baud_rate = 9600, data_bits = 8, parity = SerialPort::NONE, stop_bits = 1, handshaking = 0)
      bps = [9600, 14_400, 19_200, 38_400, 57_600, 115_200, 921_600, 1_228_739].index(baud_rate)
      pl_parity = [SerialPort::NONE, SerialPort::ODD, SerialPort::EVEN].index(parity)
      pl_write "COMM %i%i%i%i%i\r" % [bps, 8 - data_bits, pl_parity, stop_bits - 1, 0 ]#handshaking
      check_answer pl_read

      @pl.set_modem_params(baud_rate, data_bits, stop_bits, parity)

      return 0
    end
  end
end
