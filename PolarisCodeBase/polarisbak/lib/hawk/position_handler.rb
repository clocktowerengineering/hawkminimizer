require 'extendmatrix'
require 'hawk/position_generator'
require 'hawk/util/quaternion'

module Hawk

  module PositionHandler
    include PositionGenerator

    def mapToReference(targetPosition, reference)
      mappedPosition =  inverseMtMatrix(reference.position) * targetPosition
    end

    # Return a transformation matrix (distance in meters) from quaternion
    def quaternionToMatrix(q)
      a, b, c, d = q[0], q[1], q[2], q[3]
      x, y, z = q[4], q[5], q[6]
      aa = a*a
      bb = b*b
      cc = c*c
      dd = d*d

      Matrix[
        [aa + bb - cc - dd,   2*b*c - 2*a*d,     2*a*c + 2*b*d, x],
        [    2*a*d + 2*b*c, aa - bb + cc-dd,     2*c*d - 2*a*b, y],
        [    2*b*d - 2*a*c,   2*a*b + 2*c*d, aa - bb - cc + dd, z],
        [                0,               0,                 0, 1]
      ]
    end

    # Return quaternion from a transformation matrix (distance in meters)
    def matrixToQuaternion(m)
      q=Quaternion.new
      s = 0.0
      trace = 1.0 + m[0,0] + m[1,1] + m[2,2]
      threshold = 0.001

      if( trace > threshold )
          s = Math.sqrt(trace) * 2.0
          q.q1 = (m[2,1]-m[1,2]) / s
          q.q2 = (m[0,2]-m[2,0]) / s
          q.q3 = (m[1,0]-m[0,1]) / s
          q.q0 = 0.25 * s
      else
          if( m[0,0]>m[1,1] && m[0,0]>m[2,2] )
              s = Math.sqrt(1.0+m[0,0]-m[1,1]-m[2,2]) * 2.0
              q.q1 = 0.25 * s
              q.q2 = (m[0,1]+m[1,0]) / s
              q.q3 = (m[2,0]+m[0,2]) / s
              q.q0 = (m[2,1]-m[1,2]) / s
          elsif( m[1,1] > m[2,2] )
              s = Math.sqrt(1.0+m[1,1]-m[0,0]-m[2,2]) * 2.0
              q.q1 = (m[0,1]+m[1,0]) / s
              q.q2 = 0.25 * s
              q.q3 = (m[1,2]+m[2,1]) / s
              q.q0 = (m[0,2]-m[2,0]) / s
          else
              s = Math.sqrt(1.0+m[2,2]-m[0,0]-m[1,1]) * 2.0
              q.q1 = (m[2,0]+m[0,2]) / s
              q.q2 = (m[1,2]+m[2,1]) / s
              q.q3 = 0.25 * s
              q.q0 = (m[1,0]-m[0,1]) / s
          end
      end

      return q.normalize
    end

    def average_position(matrices)
      population = matrices.count
      translation = average_matrices matrices.map{|mat| mat[0..2,3] }
      rotation = average_rotMatrices matrices.map{|mat| mat[0..2,0..2]}

      Matrix[ [*rotation[0,0..2],translation[0]],
              [*rotation[1,0..2],translation[1]],
              [*rotation[2,0..2],translation[2]],
              [0, 0, 0, 1]
      ]
    end

    def inverseMtMatrix(mat)
      rot = mat[0..2,0..2].transpose
      translation = rot * (-1*mat[0..2,3])
      mat = Matrix[
        [*rot[0, 0..2], translation[0]],
        [*rot[1, 0..2], translation[1]],
        [*rot[2, 0..2], translation[2]],
        [0,0,0,1]
      ]
    end

    def average_matrices matrices
      population = matrices.count
      average = matrices.first * 0
      matrices.each do |mat|
        average += mat/population
      end
      average
    end

    def average_rotMatrices matrices
      qArray=Array.new
      population = matrices.count

      matrices.each do |mat|
        qArray.push(matrixToQuaternion(mat))
      end

      while qArray.size != 1
        a=qArray.pop(2)
        coeff=1/(population - qArray.size)
        qArray.push(a[1].slerp(a[0], coeff))
      end
      quaternion=qArray.pop
      m=quaternionToMatrix([quaternion.q0,quaternion.q1,quaternion.q2,quaternion.q3,0.0,0.0,0.0])

      return m[0..2,0..2]
    end

    def matrixToIEC(m)
      to_IEC61217_Deq0_params m
    end

  end

end
