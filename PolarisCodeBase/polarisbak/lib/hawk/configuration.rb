
require 'rconfig'
require 'hawk/manifest'


module Hawk

  DEFAULT_CONF_FILE = 'conf/main.yml'.freeze

  class Config
    class << self
      attr_reader :trackers
      attr_reader :targets
      attr_reader :references

      def load
        config_file = DEFAULT_CONF_FILE

        RConfig.load_paths = [File.dirname(config_file)]
        main = File.basename(config_file, '.yml').to_sym

        config_root = RConfig[:config_root, main] || File.dirname(config_file)
        trackers_dir = File.join(config_root, RConfig[:trackers, main].path)
        targets_dir = File.join(config_root, RConfig[:targets, main].path)
        refs_dir = File.join(config_root, RConfig[:references, main].path)

        RConfig.load_paths << trackers_dir << refs_dir

        # Remove '.' and '..'
        is_dot_file = Proc.new {|path| path.to_s.start_with? '.'}

        @trackers   = Dir[File.join(trackers_dir,"*")].map{|path| File.basename(path, '.yml').to_sym }
        @targets    = Dir[File.join(targets_dir,"*")].map{|path| File.basename(path)}
        @references = Dir[File.join(refs_dir,"*")].map{|path| File.basename(path, '.yml').to_sym }
      end

      def method_missing(method)
        RConfig.send method
      end

    end
  end
end
