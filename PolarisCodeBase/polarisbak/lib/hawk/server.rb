require 'slim'
require 'json'
require 'matrix'
require 'eventmachine'

require 'hawk/core'
require 'hawk/app/config'
require 'hawk/app/initialize'
require 'hawk/app/reference'
require 'hawk/app/status'
require 'hawk/app/select_reference'
require 'hawk/app/tracking'


module Hawk
  module Server

    def self.run!(opts={})
      EM.run do

        # define some defaults for our app
        server    = 'thin'
        host      = opts[:host]      || '0.0.0.0'
        http_port = opts[:http_port] || '8090'

        core = Core.new

        # create a base-mapping that our application will set at.
        dispatch = Rack::Builder.app do
          map '/config' do
            run Hawk::App::Config.new core
          end

          map '/initialize' do
            run Hawk::App::Initialize.new core
          end

          map '/reference' do
            run Hawk::App::Reference.new core
          end
          map '/references' do
            run Hawk::App::Reference.new core
          end

          map '/select_reference' do
            run Hawk::App::SelectReference.new core
          end

          map '/status' do
            run Hawk::App::Status.new core
          end

          map '/tracking' do
            run Hawk::App::Tracking.new core
          end
        end

        # within your EM instance.
        rack_server = Rack::Server.new(

          app:    dispatch,
          server: server,
          Host:   host,
          Port:   http_port,
          signals: false
        )

        # Configure backend "thin" server to limit simultaneous connections and
        # add threaded option
        rack_server.start do |backend|
          # Thread is not activated because it is not compatible with this
          # application. It cannot be modified easily to add this compatibily.
          #
          # backend.threaded = true
          backend.maximum_connections = 10
          backend.maximum_persistent_connections = 5
        end

      end
    end

  end

end
