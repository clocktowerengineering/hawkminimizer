require 'digest'

module Hawk

  class Manifest
    class MalFormed < StandardError; end
    class NotFound  < StandardError; end

    # Create a Manifest object and load the given manifest file given.
    def initialize manifest_file
      raise NotFound unless File.exists? manifest_file

      begin
        # Convert manifest to hash with each filename is a key and digest the value
        @manifest_map = File.readlines(manifest_file).map{|line| line.split}.to_h.invert
      rescue
        raise MalFormed
      end
    end

    # Check if the SHA256 of the given file name match with the one in the
    # manifest file.
    #
    # An array of file name can be given.
    def check files_name
      files_name = [files_name] if files_name.kind_of? String

      files_name.all?{ |file| check_digest @manifest_map[file], file }
    end

    def check_all
      @manifest_map.each_pair.all?{ |file, digest| check_digest digest, file }
    end

    def check_digest digest, filename
      digest == Digest::SHA256.file(filename).hexdigest
    end

  end

end
