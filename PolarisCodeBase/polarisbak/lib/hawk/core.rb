require 'rack'

require 'hawk/position_handler'
require 'hawk/reference_handler'
require 'hawk/polaris_handler'

module Hawk
  class Core

    include PositionHandler

    attr_reader :polaris, :references

    # Switch all handler for now. Will see in future if it still necessary.
    private :polaris

    def initialize
      trackers = Config.trackers.map{ |name| [name, RConfig.get_config_data(name)] }
      default_trackers_path = File.join("conf", RConfig.main.targets.path, RConfig.main.targets.default)
      @polaris = PolarisHandler.new(trackers, default_trackers_path)

      references = {}

      Config.references.each{ |name| references[name] = RConfig.get_config_data(name) }
      @references =  ReferenceHandler.new
      @references.init(references)

      setup

      EventMachine::PeriodicTimer.new(1) do
        tracking if setup_status.first === "SUCCESS"
      end

    end

    def setup
      polaris.initialize_polaris
    end

    def setup_status
      error = ""
      status = case polaris.setup_status
      when "Setup", "Reset"
        "INITIALIZING"
      when "Tracking"
        "SUCCESS"
      else
        "FAILED"
      end
      error = "An error occured, please check the terminal" if status == "FAILED"
      [status, error]
    end

    def status
      {polaris: polaris.status}
    end

    #Give the estimated traked position in ref, current reference is used as default
    def tracking(ref = nil)
      current_ref = ref.nil? ? references.current : references.reference(ref)

      read_positions = @polaris.target_position.select{|position| position['position'].kind_of?(Matrix)}

      if read_positions.empty?
        p 'no visible target'
        {}
      else
#         #Get the mean
#         estimated_position = average_position read_positions
#         pos_in_current_ref = mapToReference(estimated_position, current_ref)
#
#         p "Target position in #{current_ref.name}: #{[pos_in_current_ref[0,6],pos_in_current_ref[1,6],pos_in_current_ref[2,6],pos_in_current_ref[3,6],pos_in_current_ref[4,6],pos_in_current_ref[5,6]]} (data from #{read_positions.count} sensors)"
#         pos_in_current_ref

        #Get the lowest error
#         read_positions.each{|full_pos| p "#{full_pos['sensor']} see the target at #{[full_pos['position'][0,3],full_pos['position'][1,3],full_pos['position'][2,3]]} with #{full_pos['error']} error"}


#         #Get the best
#         estimated_position = read_positions.min{|a,b| a['error'] <=> b['error']}['position']
#         pos_in_current_ref = mapToReference(estimated_position, current_ref)

        read_positions.each do |pol_ans|
          pol_ans['position'] = mapToReference(pol_ans['position'], current_ref)
        end

        p "Target position in #{current_ref.name}:"
        read_positions.each{ |pol_ans| p  "  #{pol_ans['sensor']} : #{[pol_ans['position'][0,3].round(6),pol_ans['position'][1,3].round(6),pol_ans['position'][2,3].round(6)]}"}
        read_positions
      end
    end

    #References management
    def save_position_as_ref(name)
      positionRoom = tracking('Room').min{|a,b| a['error'] <=> b['error']}['position']
      if positionRoom.empty?
        p "fail to save reference due to missing target"
      else
        position = mapToReference(positionRoom, references.reference('Room'))
        references.delete name
        references.add(name, position)
      end
    end

    def select_reference name
      references.select name
    end

    def current_ref
      references.current
    end

    def get_reference name
      @references.reference name
    end

    def references_list
      @references.list
    end

    def set_position(ref_name, position)
      @references.set_position(ref_name, position)
    end

    def add(ref_name, position)
      @references.add(ref_name, position)
    end

    def delete(name)
      @references.delete name
    end

  end
end
