require 'hawk/polaris_driver_usb'
require 'hawk/polaris_driver_eth'
require 'hawk/position_handler'
require 'delegate'
require 'matrix'
require 'json'
require 'net/http'
require 'uri'

# This version of Polaris.rb adds the following features:
#
# 1) Moving vs not moving check.  System checks whether the square of the root sum square (ie the sum square) of the difference between the current reported position and the value of the position @averagepositions ago is greater than @movingcriterionsquared.  If it is greater or the target is missing, the target is moving, so set @movingtarget to true and reset the @stationarycounter to 0.  If it is not greater, increment @stationarycounter, and if @stationarycounter is greater than @stationarycriteria, set @moving to false.  This functionality is enabled by setting @movingcheck to true.

# 2) target reporting behavior change for occlusions.  If @moving is true, report values the usual way: if tracking, return the target position, if not tracking, return MISSING, if error, return error.  If @moving is false, if the camera is tracking, pop the oldest value of target positions from the @targetvalues buffer and push the latest value, then report the average value of the last @averagepositions of @targetvalues buffer.  If camera is not tracking, report the average value of the last @averagepositions of @targetvalues buffer.  If error, report error.

# 3) flag @averageifmoving.  if @averageifmoving is true, while @moving, if tracking, return the average position over the last @averagepositions, otherwise, return the last position.

# 4) flag @movingcompareaverage.  if @movingcompareaverage is true, compare the present to the average position, if false, compare to the oldest value in the @targetvalues buffer.

# 5) PPS moving check.  System checks whether the PPS is presently in the READY state.  if it isn't, @movingtarget is treated as true.  the pps moving check can be enabled by setting @ppsmovingcheck to true.


module Hawk
  class Polaris < Delegator
    include PositionHandler

    attr_reader :baudrate, :position, :driver, :port_handle, :tool

    alias :__getobj__ :driver

    def initialize(device, baudrate, position, target_file)
      @driver = ((/(\d{1,3}\.){3}\d{1,3}/ =~ device) ? (PolarisDriverTCP.new device) : (PolarisDriverSerial.new device))
      @position = position
      @baudrate = baudrate
      @movingtarget = true
      @movingcheck = true
      @stationarycounter = 0
      @stationarycriteria = 10
      @movingcriterionsquared = 0.000001
      @averageifmoving = false
      @movingcompareaverage = false
      @averagepositions = 10
      @targetvalues = Array.new(8) {Array.new(@averagepositions)}
      @goodcounter = @goodtrigger
      @targetaverage = Array.new(8)
      @ppsmovingcheck = true
      @uri = URI.parse("http://192.168.104.161:8081/api")
      @tool   = File.open(target_file).read
    end
    
    def init
      driver.reset
      driver.comm_settings baudrate if driver.class == PolarisDriverSerial
      driver.init
      driver.set("Param.Tracking.Selected Volume=1")
      driver.set("Param.Tracking.Sensitivity=1")

      @port_handle = driver.assign_ph

      driver.load_tool_file port_handle, tool
      driver.init_ph port_handle
      driver.enable_ph port_handle
    end

    # Read polaris information and return the target position
    # in the Fixed Reference System as a 4x4 matrix in SI
    def target_position
      target = driver.get_tool_trans[port_handle].first

	  
      # Behavior #5 implemented here.
      
      
      if @ppsmovingcheck

        request = Net::HTTP::Post.new(@uri)
        request.content_type = "application/json"
        request["Accept"] = "application/json"
        request.body = JSON.dump({
          "command" => "getStatus"
        })

        req_options = {
          use_ssl: @uri.scheme == "https",
        }

        response = Net::HTTP.start(@uri.hostname, @uri.port, req_options) do |http|
          http.request(request)
        end

        if( JSON.parse(response.body)["state"] != "READY")
          puts JSON.parse(response.body)["state"]
          @movingtarget = true
          @stationarycounter = 0
        end
      
      end
      

      # behaviors #1 and #4 implemented here.
    
    
      # begin movingcheck.
      #puts @movingtarget
      if @movingtarget
      
        if (target.first == 'MISSING')
        
          # missing target, must be moving. reset the stationarycounter.
          @stationarycounter = 0
          @movingtarget = true
          
        else # ok, we have a target, let's do the moving check.
        
          # do the math of the sum squared difference either against the oldest value or the average value.  note that we are just using the quaternion spatial values instead of converting into the correct reference frame because the sum square difference of 3D spatial values is the same regardless of what reference we are in.  saves some math and some processing time.
          
          if @movingcompareaverage
            
            
            # do the math against the average, not the oldest value.
            currenterror = (@targetaverage[4].to_f-target[4].to_f)**2 + (@targetaverage[5].to_f-target[5].to_f)**2 + (@targetaverage[6].to_f-target[6].to_f)**2
            
            #puts currenterror
            
            
          else
          
            # do the math against the oldest value, not the average.
            currenterror = (@targetvalues[0][4].to_f-target[4].to_f)**2 + (@targetvalues[0][5].to_f-target[5].to_f)**2 + (@targetvalues[0][6].to_f-target[6].to_f)**2
            
          end
          
          # behavior 4 implemented here:
          
          
          # compare the currenterror against @movingcriterionsquared
          # also implement behvaior #4, if we aren't checking moving set @movingtarget
          
          if @movingcriterionsquared > currenterror or @movingcheck == false
          
            # system is moving.
            @movingtarget = true
            @stationarycounter = 0
          
          else
          
            #system may not be moving!  increment the counter.
            
            @stationarycounter = @stationarycounter + 1
            
            # check whether we've been stationary for long enough
            
            if @stationarycounter > @stationarycriteria
            
              #cool, we're stationary!
              
              @stationarycounter = @stationarycriteria
              @movingtarget = false
              
              
            end
          
          end
        
        end
      
      end
      
    
      # behavior #2 implemented here.
    
      if @movingtarget
    
      # target is moving.
      
        if (target.first == 'MISSING')
          
          # target is missing.
          return 'MISSING'
          
          
        else
        
        # target is not missing.  we have to report something back to the rest of the HAWK system.
      
        # routine time: we want to have the option of averaging the last @averagepositions positions and use that as the quaternion value instead of the raw last quaternion.
        
        # method:
        # 
        # 1) append the newest value of target[n] to @targetvalues[n].
        # 2) check whether the length of @targetaverage[n] is greater than @averagepositions.
        # 3) if it is, remove the oldest value from @targetvalues[n].
        # 4) take the average of @targetvalues[n] for each [n] and store that average value to targetaverage[n].
        
        # 1) push the newest value of target[n] to @targetvalues[n]. 


          for n in 0..7
            @targetvalues[n].push(target[n])
          
          # 2) check whether the length of @targetaverage[n] is greater than @averagepositions.
          
           
            if @targetvalues[n].size > @averagepositions
              # 3) if it is, remove the oldest value from @targetvalues[n]. 
            
              # to do this, for each n in @targetvalues[n], shift the array by one to remove the oldest sample.
              @targetvalues[n].shift

                   
            end
            
            # 4) take the average of @targetvalues[n] for each [n] and store that average value to @targetaverage[n].
            
            @targetaverage[n] = @targetvalues[n].compact.reduce(:+) / @targetvalues[n].compact.size.to_f
            
          end
        
        
          # behavior #3 implemented here: ok, now either report the present value or the averaged value.
        
          if @averageifmoving
          
            qTaPo = @targetaverage[0..3] + @targetaverage[4..6].map {|val| val / 1000.0}
            mTaPo = quaternionToMatrix Vector.elements(qTaPo)
            {'position' => Matrix[*position] * mTaPo,
            'error' => @targetaverage[7]}

            
          else
          
            qTaPo = target[0..3] + target[4..6].map {|val| val / 1000.0}
            mTaPo = quaternionToMatrix Vector.elements(qTaPo)
            {'position' => Matrix[*position] * mTaPo,
            'error' => target[7]}
            
          end
        

        
        end
        
        
      # target is NOT moving!
      
      else
        
        if (target.first != 'MISSING')
        
          #target isn't missing.  update the average and report it.
        
          for n in 0..7
            @targetvalues[n].push(target[n])
          
          # 2) check whether the length of @targetaverage[n] is greater than @averagepositions.
          
           
            if @targetvalues[n].size > @averagepositions
              # 3) if it is, remove the oldest value from @targetvalues[n]. 
            
              # to do this, for each n in @targetvalues[n], shift the array by one to remove the oldest sample.
              @targetvalues[n].shift

                   
            end
            
            # 4) take the average of @targetvalues[n] for each [n] and store that average value to @targetaverage[n].
            
            @targetaverage[n] = @targetvalues[n].compact.reduce(:+) / @targetvalues[n].compact.size.to_f
            
          end
      

        
        end
        # if the target is missing, who cares, just do the math on the present value set
        qTaPo = @targetaverage[0..3] + @targetaverage[4..6].map {|val| val / 1000.0}
        mTaPo = quaternionToMatrix Vector.elements(qTaPo)
        {'position' => Matrix[*position] * mTaPo,
        'error' => @targetaverage[7]}
      
	
      end
    
    end
    

    # Return the operating mode.
    # Possible result are "Setup", "Reset", "Tracking" and "Diagnostic".
    def mode
      driver.get("Info.Status.System Mode")
    end

  end
end
