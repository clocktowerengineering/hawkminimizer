require 'hawk/configuration'
require 'hawk/command_handler'
require 'hawk/polaris_driver'
require 'sinatra/base'
require 'slim'
require 'json'
require 'matrix'

#         /!\
#       // ! \\ CECI EST UN EXEMPLE ET PAS UNE CLASSE A UTILISÉE RÉELEMENT !
#     ///__!__\\\

module Hawk

  def position(polaris, ph)
    target = polaris.get_tool_trans[ph].first

    if target.first == "MISSING"
      return nil
    else
      return Vector.elements target[4, 3].map {|v| v/1000.0}
    end
  end

  class HttpServer < Sinatra::Base
    configure do
      puts "Starting init"
      set :polaris, PolarisDriver.new(Config.polaris1.device)
    end

    post '/initialize' do
      settings.polaris.reset
      settings.polaris.comm_settings 115_200
      settings.polaris.init

      ph = settings.polaris.assign_ph
      tool = File.open("conf/mP_I-Ring_Protom.rom").read

      settings.polaris.load_tool_file ph, tool
      settings.polaris.init_ph ph
      settings.polaris.enable_ph ph
      settings.polaris.start_tracking true

      @reference_trans = position(settings.polaris, ph)

      h = if @reference_trans.nil?
        { "status" => "INITIALIZE_FAIL" }
      else
        { "status" => "INITIALIZE_SUCCESS" }
      end

      JSON.generate h
    end

    get '/tracking' do
      content_type 'application/json'

      id = request.ip
      s = request.body.read

      trans = position(settings.polaris, 1)

      h = case
  #     when @reference_trans.nil?
  #       { "status" => "TRACKINGSTATUS_INITIALIZEFAIL" }
      when trans.nil?
        { "status" => "TARGET_MISSING" }
      else
        {
          "position" => trans.to_a,
          "status" => "SUCCESSFUL"
        }
      end

      JSON.generate h
    end

  end

end