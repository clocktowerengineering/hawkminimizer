require 'matrix'



    # Create an homogenous matrix 4×4 with rotation 3×3 matrix and a translation
    # column vector
    #
    # [     x
    #  3×3  y
    #     ] z
    # 0 0 0 1
    def homogenousMatrix(r, t)
      r.vstack(Matrix[[0, 0, 0]]).hstack Matrix.column_vector(t.to_a << 1)
    end

    # Returns rotation 3×3 matrix and translation vector from homogenous matrix.
    # @param m must be a Matrix
    def splitHomogenousMatrix(m)
      r = m.minor(0, 3, 0, 3)
      t = m.minor(0, 3, 3, 1).column(0)

      [r, t]
    end

    # Returns an Array of all independent matrices. First 3 matrices are Rx, Ry,
    # Rz rotation and the last one for translation.
    def independentMatrices(x, y, z, rx, ry, rz)
      [rot4x(rx), rot4y(ry), rot4z(rz), transl(x, y, z)]
    end

    # @return rotation matrix associated with a rotation around *X* axis
    def rotx(psi)
      Matrix[
        [1,             0,              0],
        [0, Math.cos(psi), -Math.sin(psi)],
        [0, Math.sin(psi),  Math.cos(psi)]
      ]
    end

    # @return rotation matrix associated with a rotation around *Y* axis
    def roty(theta)
      Matrix[
        [ Math.cos(theta), 0, Math.sin(theta)],
        [               0, 1,               0],
        [-Math.sin(theta), 0, Math.cos(theta)]
      ]
    end

    # @return rotation matrix associated with a rotation around *Z* axis
    def rotz(phi)
      Matrix[
        [Math.cos(phi), -Math.sin(phi), 0],
        [Math.sin(phi),  Math.cos(phi), 0],
        [            0,              0, 1]
      ]
    end

    # @param psi, theta, phi : Angles in RADIANS, for *counter-clockwise* euler rotations x, y, z.
    # @return Rxyz so that Rxyz*V(expressed in former reference) = V(expressed in rotated reference).
    def generate_rotation_matrix_eulerXYZ(psi, theta, phi)
      return rotx(psi)*roty(theta)*rotz(phi)
    end

    # @return Rzyx, a rotation matrix from eulers angle.
    # @see generate_rotation_matrix_eulerXYZ
    def generate_rotation_matrix_eulerZYX(psi, theta, phi)
      return rotz(phi)*roty(theta)*rotx(psi)
    end

    # @return Rzxy, a rotation matrix from eulers angle.
    # @see generate_rotation_matrix_eulerXYZ
    def generate_rotation_matrix_eulerZXY(psi, theta, phi)
      return rotz(phi)*rotx(psi)*roty(theta)
    end

    # @return Ryxz, a rotation matrix from eulers angle.
    # @see generate_rotation_matrix_eulerXYZ
    def generate_rotation_matrix_eulerYXZ(psi, theta, phi)
      return roty(theta)*rotx(psi)*rotz(phi)
    end

    def generate_transformation_matrix_eulerXYZ(x, y, z, psi, theta, phi)
      mx, my, mz, tr = independentMatrices(x, y, z, psi, theta, phi)
      return tr*mx*my*mz
    end

    # @return rotation around *X* axis as a 4x4 transformation matrix
    def rot4x(psi)
      return homogenousMatrix(rotx(psi), [0, 0, 0])
    end

    # @return rotation around *Y* axis as a 4x4 transformation matrix
    def rot4y(theta)
      return homogenousMatrix(roty(theta), [0, 0, 0])
    end

    # @return rotation around *Z* axis as a 4x4 transformation matrix
    def rot4z(phi)
      return homogenousMatrix(rotz(phi), [0, 0, 0])
    end

    # @return the translation as a 4x4 transformation matrix
    def transl(x, y, z)
      homogenousMatrix(Matrix.I(3), [x, y, z])
    end

    # @brief Generate Isocentric transformation matrix from the six parameters
    # defined in norm. (Rz then Ry then Rx then translation)
    #
    # @return a 4x4 transformation matrix
    def generate_isocentric(x, y, z, thetaX, thetaY, thetaZ)
      mx, my, mz, tr = independentMatrices(x, y, z, thetaX, thetaY, thetaZ)

      mz*my*mx*tr
    end

    # @brief Retrieve Isocentric parameters (ie parameters giving tool (TTCS)
    # position in FRS coordinate system), from classic Euler angles and
    # translation expressed in FRS.
    #
    # @param m [Matrix] the transformation matrix passing from TTCS to FRS
    # @return array of six parameters : [x, y, z, thetaX, thetaY, thetaZ],
    # according to IBA document mentioned above.
    def to_isocentric_params(m)
      h = if m.kind_of?(Matrix) then m else Matrix.rows(m) end
      return rt_to_isocentric_params *splitHomogenousMatrix(h)
    end

    # @brief Retrieve Isocentric parameters (ie parameters giving tool (TTCS)
    # position in FRS coordinate system), from classic Euler angles and
    # translation expressed in FRS.
    #
    # @param m [Matrix] the transformation matrix passing from TTCS to FRS.
    # @return array of six parameters : [x, y, z, thetaX, thetaY, thetaZ],
    # according to IBA document mentioned above.
    #
    # See IBA Document 11197 Rev.D : *Specification of coordinate systems for
    # transfer of information between system components.* for more information
    # about norms.
    def rt_to_isocentric_params(r, t)
      rx, ry, rz = basic_euler_angles_from_rotation_matrixZYX(r.to_a)
      tinit = (rotz(rz)*roty(ry)*rotx(rx)).transpose() * Vector.elements(t)

      return tinit.to_a.concat([rx, ry, rz])
    end

    # @brief Generate IEC61217 transformation matrix from the six parameters
    # defined in norm.
    #
    # A seventh parameter D, is choosen equal to zero, following IBA
    # specifications. Angles arguments are given in a strange order following
    # IBA document : Rotation, Roll, Pitch.
    #
    # See IBA Document 11197 Rev.D : *Specification of coordinate systems for
    # transfer of information between system components.* for more information
    # about the norm and conventions used.
    #
    # @return A 4*4 transformation matrix.
    def generate_IEC61217_Deq0(x, y, z, thetaX, thetaY, thetaZ)
      mx, my, mz, tr = independentMatrices(x, y, z, thetaX, thetaY, thetaZ)
      mz*tr*mx*my
    end

    # @brief Retrieve IEC61217 parameters (ie parameters giving tool (TTCS)
    # position in FRS coordinate system), from classic eulers angles and
    # translation expressed in FRS.
    #
    # @param m [Matrix] the transformation matrix passing from TTCS to FRS
    #
    # @return array of six parameters : [x, y, z, thetaX, thetaY, thetaZ],
    # according to IBA document mentionned above.
    #
    # See IBA Document 11197 Rev.D : *Specification of coordinate systems for
    # transfer of information between system components.* for more information
    # about norms.
    def to_IEC61217_Deq0_params(m)
      h = if m.kind_of?(Matrix) then m else Matrix.rows(m) end
      return rt_to_IEC61217_Deq0_params *splitHomogenousMatrix(h)
    end

    # @brief Retrieve IEC61217 parameters (ie parameters giving tool (TTCS)
    # position in FRS coordinate system), from classic eulers angles and
    # translation expressed in FRS.
    #
    # @param m [Matrix] the transformation matrix passing from TTCS to FRS
    # @return array of six parameters : [x, y, z, thetaX, thetaY, thetaZ],
    # according to IBA document mentionned above.
    # See IBA Document 11197 Rev.D : *Specification of coordinate systems for
    # transfer of information between system components.* for more information
    # about norms.
    def rt_to_IEC61217_Deq0_params(r, t)
      rx, ry, rz = basic_euler_angles_from_rotation_matrixZXY(r.to_a)
      tinit = rotz(rz).transpose() * Vector.elements(t)

      return tinit.to_a.concat([rx, ry, rz])
    end

    # @return [Array] Angles in RADIANS, *counter-clockwise rotations* [rx, ry, rz]
    # Implemented following : https://truesculpt.googlecode.com/hg-history/38000e9dfece971460473d5788c235fbbe82f31b/Doc/rotation_matrix_to_euler.pdf
    # Two solutions are existing, but we are choosing (by convention) to use the
    # solution where Ry is in the [-Pi/2, Pi/2] interval.
    #
    # Gimbal lock is treated by setting Rx to 0
    def basic_euler_angles_from_rotation_matrixZYX(rot_mat, tolerance=1e-10)
      r = Matrix.rows(rot_mat)
      #     raise OrionWarning.new("AP-MATH-MATRIX_INPUT", "Input matrix isn't a rotation matrix. Cant convert it to euler's angle.") unless r.belongs_to_SO3?(tolerance)
      if( r[2,0].abs != 1 )
        rx1 = Math.atan2(r[2,1], r[2,2])
        # rx2 = Math.atan2(r[2,1]/Math.cos(ry2), r[2,2]/Math.cos(ry2))
        ry1 = -Math.asin(r[2,0])
        # ry2 = PI - ry1
        rz1 = Math.atan2(r[1,0], r[0,0])
        # rz2 = Math.atan2(r[1,0]/Math.cos(ry2), r[0,0]/Math.cos(ry2))
        return [rx1, ry1, rz1]

      else # see Gimbal lock on wikipedia.
        rx = 0 # anything, set to 0 for convenience and unique solution
        if r[2,0] == -1
          ry = Math::PI/2.0
          rz = rx - Math.atan2(r[0,1], r[0,2])
        else
          ry = -Math::PI/2.0
          rz = Math.atan2(-r[0,1], -r[0,2]) - rx
        end
        return [rx, ry, rz]
      end
    end

    def basic_euler_angles_from_rotation_matrixXYZ(rot_mat)
      r = Matrix.rows(rot_mat)
      rx = -Math.atan2( r[1,2] , r[2,2] )
      ry =  Math.asin( r[0,2] )
      rz = -Math.atan2( r[0,1] , r[0,0] );
      return [rx, ry, rz]
    end

    # @return [Array] Angles in RADIANS, *counter-clockwise rotations*. [rx, ry, rz]
    # Implemented modifying : https://truesculpt.googlecode.com/hg-history/38000e9dfece971460473d5788c235fbbe82f31b/Doc/rotation_matrix_to_euler.pdf
    # Two solutions are existing, but we are choosing (by convention) to use the
    # solution where Rx is in the [-Pi/2, Pi/2] interval
    #
    # Gimbal lock is treated by setting Rx to 0
    def basic_euler_angles_from_rotation_matrixZXY(rot_mat)
      r = Matrix.rows(rot_mat)
      #     raise OrionWarning.new("AP-MATH-MATRIX_INPUT", "Input matrix isn't a rotation matrix. Cant convert it to YXZ euler's angle.") unless r.belongs_to_SO3?(1e-10)
      if( r[2,1].abs != 1 )
        rx1 = Math.asin(r[2,1])
        ry1 = Math.atan2(-r[2,0], r[2,2])
        rz1 = Math.atan2(-r[0,1], r[1,1])
        return [rx1, ry1, rz1]
      else # see Gimbal lock on wikipedia.
        rz = 0 # anything, set to 0 for convenience and unique solution
        if r[2,1] == 1
          rx = Math::PI/2.0
          ry = Math.atan2(r[1,0], r[0,0]) - rz
        else
          rx = -Math::PI/2.0
          ry = -Math.atan2(r[1,0], r[0,0]) + rz
        end
        return [rx, ry, rz]
      end
    end

    # @return [Array] Angles in RADIANS, *counter-clockwise rotations*.
    # [rx, ry, rz] we chose arbitrary rx=0, x axis of the new coordinate system is
    # pointing in the same direction as vector
    def basic_euler_angles_from_vectorXYZ(vector)
      raise "Input vector isn't valid. Cant convert it to euler's angle." unless vector.size == 3

      x=vector[0]
      y=vector[1]
      z=vector[2]

      rx =  0.0
      ry = -Math.atan2(z,x)
      rz = Math.atan2(y,Math.sqrt(x**2+z**2))

      return [rx, ry, rz]
    end

    # @return [Array] Angles in RADIANS, *counter-clockwise rotations*.
    # [rx, ry, rz] we chose arbitrary rx=0, x axis of the new coordinate system is
    # pointing in the same direction as vector
    def basic_euler_angles_from_vectorZYX(vector)
      raise "Input vector isn't valid. Cant convert it to euler's angle." unless vector.size == 3

      x=vector[0]
      y=vector[1]
      z=vector[2]

      rx=0.0
      ry=-Math.atan2(z,Math.sqrt(x**2+y**2))
      rz=Math.atan2(y,x)

      return [rx, ry, rz]
    end

    # @return [Array] of randomly generated numbers int he range [-range, +range]
    def generate_random_cartesian_position(ranges)
      prng = Random.new
      position=Array.new(ranges.count){|i| prng.rand(-ranges[i]..ranges[i])}
      return position
    end

    # Angles in *DEGREES*.
    # Randomly generate a rotation matrix from randomly generated euler's angles.
    # Randomly generated angles stay in the range +-psi_max_deg, +-theta_max_deg,
    # +-phi_max_deg
    def generate_random_eulerXYZ(ranges)
      prng  = Random.new
      position=Array.new(ranges.count){|i| prng.rand(-ranges[i].to_rad..ranges[i].to_rad)}
      return position
    end

    # Compute rotation matrix from a rodrigues rotation vector a a rotation angle
    # @param vector [Array] an 3*1 array indicating axis we are rotating around.
    # No units, it will be normalized.
    #
    # @param phi [Float] angle of rotation. In radians.
    # @see [wikipedia](http://fr.wikipedia.org/wiki/Rotation_vectorielle#Rotation_vectorielle_dans_l.27espace_de_dimension_3)
    def rodriguesToMatrix(vector, phi)
      n = Vector.elements(vector).normalize
      nx = n[0]; ny=n[1]; nz=n[2]

      r = Math.cos(phi)*Matrix[[1.0,0.0,0.0],
                               [0.0,1.0,0.0],
                               [0.0,0.0,1.0]] +
                               (1-Math.cos(phi))*Matrix[[nx*nx, nx*ny, nx*nz],
                                                        [nx*ny, ny*ny, ny*nz],
                                                        [nx*nz, ny*nz, nz*nz]] +
                                                        Math.sin(phi)*Matrix[[0.0, -nz, ny],
                                                                             [nz, 0.0, -nx],
                                                                             [-ny, nx, 0.0]]
      return r
    end

    # Create a 4×4 matrix for an position vector. Position is a vector define with
    # [ x, y, z, Rx, Ry, Rz ]. A convention string parameter can be passed to
    # specify in which convention the input position vector is defined. For now
    # "eulerXYZ" and "IEC61217" are possible. Convention strings are not case
    # sensitive.
    #
    # If convention is not known or not specified the default "IEC61217" is used.
    def conventionToMatrix(position, convention = nil)
      case convention.downcase
      when "eulerxyz"
        generate_transformation_matrix_eulerXYZ(*(position)).to_a
      when "iec"
        generate_IEC61217_Deq0(*position).to_a
      else
        generate_IEC61217_Deq0(*position).to_a
      end
    end

    # Convert a array Matrix to separate Matrix for rotation and a vector for
    # translation.
    #
    # "r" represents the rotation matrix and "t" the translation vector.
    # The output matrix is a hash like:
    #
    # {
    #   "r": [[ 0, 0, 0], [0, 0, 0], [0, 0, 0]]
    #   "t": { "x": 0, "y":0, "z": 0}
    # }
    def matrixToRTMatrix(matrix)
      matrix.flatten!
      t = matrix.values_at(3, 7, 11)
      r = matrix.values_at(0..2, 4..6, 8..10)

      { "r" => r, "t"  => { "x" => t[0], "y" => t[1], "z" => t[2] }}
    end

  

hmmmm = generate_IEC61217_Deq0(1,2,3,0.25,0.5,0.75)
puts hmmmm
aaaaa = to_IEC61217_Deq0_params(hmmmm)
puts aaaaa

