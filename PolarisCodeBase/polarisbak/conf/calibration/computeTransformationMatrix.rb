require 'matrix'
require 'json'

p ARGV[0]
p ARGV[1]

p1=JSON.parse(File.read(ARGV[0]))["position"].map{ |arr| arr.map{ |v| v.to_f } }
p2=JSON.parse(File.read(ARGV[1]))["position"].map{ |arr| arr.map{ |v| v.to_f } }

matP1=Matrix[*p1]
matP2=Matrix[*p2]

res = matP2 * matP1.inverse

File.open("newTransformation.json", "w+") {|f| f.write(res) }
