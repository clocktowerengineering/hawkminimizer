%Guide d'utilisation de ce script dans "hawk/doc/procédure de caractérisation position polaris.rst" 

#robot
q1 = [ 0.4181258101577897, -0.8727389776366778, -0.1338069778619943] ;
q2 = [  0.05626387162972733, -0.9186849376244113, -0.08673677332003815];
q3 = [ -0.07702874399008615, -0.6036832811408432, 0.13235502925453468];


#polaris
p1 = [0.07623, 0.18559, -1.7584];
p2 = [0.32775, 0.00131, -1.95603];
p3 = [0.43169, -0.33666, -1.74625];


function[q] = toquaternion(m)

  diag = [abs(m(1,1)), abs(m(2,2)), abs(m(3,3))];

  if( max(diag) == abs(m(1,1)) )
    u = 1, v = 2, w = 3;
  elseif( max(diag) == abs(m(2,2)) )
    u = 2, v = 3, w = 1;
  elseif( max(diag) == abs(m(3,3)) )
    u = 3, v = 1, w = 2;
  endif

  r =  sqrt(1 + m(u,u) - m(v,v) - m(w,w));
  q0 = ( m(w,v) - m(v,w) ) / (2 * r);
  Q(u) = r / 2;
  Q(v) = ( m(u,v) + m(v,u) ) / (2 * r);
  Q(w) = ( m(w,u) + m(u,w) ) / (2 * r);

  q = [ q0 Q];

endfunction

function[M, M0] = match_frame(p1, p2, p3, q1, q2, q3)

  p32 = -p3 + p2;
  p31 = -p3 + p1;
  Az = cross(p32, p31);
  Az = Az / norm(Az);
  Ax = p32 / norm(p32);
  Ay = cross(Az, Ax);
  A0 = p3;
  A = [ Ax ; Ay ; Az ];

  q32 = -q3 + q2;
  q31 = -q3 + q1;
  Bz = cross(q32, q31);
  Bz = Bz / norm(Bz);
  Bx = q32 / norm(q32);
  By = cross(Bz, Bx);
  B0 = q3;
  B = [ Bx ; By ; Bz ];

  M = inv(B) * A;
  M0 = B0 - transpose(M * transpose(A0));

endfunction

function[Rx, Ry, Rz] = to_euler(M)
  Rx = atan2(M(3,2), M(3,3))*180/pi;
  Ry = atan2(-M(3,1), sqrt(M(3,2)^2+M(3,3)^2))*180/pi;
  Rz = atan2(M(2,1), M(1,1))*180/pi;
endfunction


[M, M0] = match_frame(p1, p2, p3, q1, q2, q3)
[Rx, Ry, Rz] = to_euler(M)
Mp = [M [M0(1) ; M0(2) ; M0(3)] ; 0 0 0 1]
Mpinv = inv(Mp)

q = toquaternion(M)
norm(q)

% K = [    M(1,1)-M(2,2)-M(3,3), M(2,1)+M(1,2), M(3,1)+M(1,3), M(2,3)-M(3,2)];
% K = [ K; M(2,1)+M(1,2), M(2,2)-M(1,1)-M(3,3), M(3,2)+M(2,3), M(3,1)-M(1,3)];
% K = [ K; M(3,1)+M(1,3), M(3,2)+M(2,3), M(3,3)-M(1,1)-M(2,2), M(1,2)-M(2,1)];
% K = [ K; M(2,3)-M(3,2), M(3,1)-M(1,3), M(1,2)-M(2,1), M(1,1)+M(2,2)+M(3,3)];

% K=K/3;
% [v,d] = eig(K);


