function isomatrix = generate_isocentric(inputvector)

  
  [mx, my, mz, tr] =  independentMatrices(inputvector);
  
  isomatrix = mz*my*mx*tr;
  
endfunction
