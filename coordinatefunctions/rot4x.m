function rotationmatrix = rot4x(needspadding)
  rotationmatrix = rotx(needspadding);
  rotationmatrix(4,4) = 1;

endfunction
