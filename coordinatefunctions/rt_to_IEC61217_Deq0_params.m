function IECMatrix = rt_to_IEC61217_Deq0_params(r)
  
  [rx,ry,rz] = basic_euler_angles_from_rotation_matrixZXY(r);
   x = r(1,4);
   y = r(2,4);
   z = r(3,4);
   value = transpose(rotz(rz)) * [x;y;z];
  
   IECMatrix = [value(1),value(2),value(3),rx,ry,rz];
  
endfunction
#something isn't quite right with this
