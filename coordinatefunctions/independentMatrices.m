    # Returns an Array of all independent matrices. First 3 matrices are Rx, Ry,
    # Rz rotation and the last one for translation.

function [mx,my,mz,tr ] = independentMatrices(inputvector)
  x = inputvector(1);
  y = inputvector(2);
  z = inputvector(3);
  rx = inputvector(4);
  ry = inputvector(5);
  rz = inputvector(6);  
  mx = rot4x(rx);
  my = rot4y(ry);
  mz = rot4z(rz);
  tr = transl(x,y,z);
  
endfunction
