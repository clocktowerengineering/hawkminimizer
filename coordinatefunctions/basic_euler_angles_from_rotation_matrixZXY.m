function [rx,ry,rz] = basic_euler_angles_from_rotation_matrixZXY(rot_mat)
  
  r = rot_mat;
  
  if abs(r(3,2)) ~=1
    
    rx = asin(r(3,2));
    ry = atan2(-r(3,1),r(3,3));
    rz = atan2(-r(1,2),r(2,2));
    
  else
    
    rz = 0
    if r(3,2) == -1
      
      rx = pi/2;
      ry = atan2(r(2,1),r(1,1))-rz;
      
    else
      
      rx = -pi/2;
      ry = atan2(r(2,1),r(1,1))+rz     ; 
      
    end
  end
  
  
  
  
endfunction
