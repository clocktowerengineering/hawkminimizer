function rotationmatrix = roty(thetaangle)
  
  rotationmatrix = [[ cos(thetaangle), 0, sin(thetaangle)]; [0, 1,0]; [-sin(thetaangle), 0, cos(thetaangle)]];
endfunction
