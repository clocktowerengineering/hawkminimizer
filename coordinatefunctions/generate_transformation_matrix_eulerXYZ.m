function eulermatrix = generate_transformation_matrix_eulerXYZ(inputvector)

  
  [mx, my, mz, tr] =  independentMatrices(inputvector);
  
  eulermatrix = tr*mx*my*mz;
  
endfunction
