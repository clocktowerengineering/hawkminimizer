function rotationmatrix = rot4z(needspadding)
  rotationmatrix = rotz(needspadding);
  rotationmatrix(4,4) = 1;

endfunction
