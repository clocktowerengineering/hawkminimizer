# return a translation of xyz as a 4x4.
function translationmatrix = transl(x,y,z)
  translationmatrix = eye(4);
  translationmatrix(1,4) = x;
  translationmatrix(2,4) = y;
  translationmatrix(3,4) = z;
  translationmatrix(4,4) = 1;
endfunction
