function rotationmatrix = rotx(psiangle)
  
  rotationmatrix = [[1,0, 0];[0, cos(psiangle), -sin(psiangle)];[0,sin(psiangle),cos(psiangle)]]; 
  
endfunction
