function IECMatrix = generate_IEC61217_Deq0(inputvector)

  
  [mx, my, mz, tr] =  independentMatrices(inputvector);
  
  IECMatrix = mz*tr*mx*my;
  
endfunction
