function rotationmatrix = rotz(phiangle)
  
  rotationmatrix = [[cos(phiangle), -sin(phiangle), 0];[sin(phiangle),  cos(phiangle), 0];[0,0,1]];
endfunction
