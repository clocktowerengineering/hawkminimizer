function [rx,ry,rz] = basic_euler_angles_from_rotation_matrixZYX(rot_mat)
  
  r = rot_mat;
  
  if abs(r(3,1)) ~=1
    
    rx = atan2(r(3,2),r(3,3));
    ry = -asin(r(3,1));
    rz = atan2(r(2,1),r(1,1));
    
  else
    
    rx = 0;
    if r(3,1) == -1
      
      ry = pi/2;
      rz = rx - atan2(r(1,2),r(1,3));
      
    else
      
      ry = -pi/2;
      rz = atan2(-r(1,2),-r(1,3)) - rx;
      
      
    end
  end
  
  
  
  
endfunction
