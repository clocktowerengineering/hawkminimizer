function rotationmatrix = rot4y(needspadding)
  rotationmatrix = roty(needspadding);
  rotationmatrix(4,4) = 1;

endfunction

