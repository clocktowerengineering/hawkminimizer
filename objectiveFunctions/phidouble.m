function obj = phidouble(x)
  
  # here's the phi function for optimization with a twist: it checks position on both cameras at the same time!  HMM.
  
  # take the input x vector and check that it's a length 6 vector.
  
  if length(x) ~=12
    disp 'BAD INITIAL GUESS!!!'
     
  end
  
  #ok, it's a good vector.  convert vector to euler.
  
  aquilafourbyx = generate_transformation_matrix_eulerXYZ(x(1:6));
  falcofourbyx = generate_transformation_matrix_eulerXYZ(x(7:12));
  
  
  # grab globals.
  capturedpoints = evalin('base','capturedpoints');
  aquilacameraposition = evalin('base','aquilacameraposition');
  falcocameraposition = evalin('base','falcocameraposition');
  whichcamera = evalin('base','correctwhichcamera');
  
  if strcmp(whichcamera,'falco')
  
    # okay, calculate new positions.
    for i = 1: length(capturedpoints)
      capturedpoints(i).newfalco = falcofourbyx * falcocameraposition^-1 * capturedpoints(i).falco;
      
    endfor

    [meritvalue,meritarray]=calcMeritFunction(capturedpoints);
    obj = meritvalue;
    
  elseif strcmp(whichcamera,'aquila')
     # okay, calculate new positions.
    for i = 1: length(capturedpoints)
      capturedpoints(i).newaquila = aquilafourbyx * aquilacameraposition^-1 * capturedpoints(i).aquila;
      
    endfor

    [meritvalue,meritarray]=calcMeritFunction(capturedpoints);
    obj = meritvalue;
    
  else
    #whoo, run a double optimization 
     # okay, calculate new positions.
    for i = 1: length(capturedpoints)
      capturedpoints(i).newaquila = aquilafourbyx * aquilacameraposition^-1 * capturedpoints(i).aquila;
      capturedpoints(i).newfalco = falcofourbyx * falcocameraposition^-1 * capturedpoints(i).falco;
    endfor

    [meritvalue,meritarray]=calcMeritFunction(capturedpoints);
    obj = meritvalue;
    
  endif
  

endfunction
