function obj = cameraToPPSSolver(x)
  
  # this function takes a single 1x12 vector, splits them into two 1x6 vectors,
  # treats them as IEC vectors, then computes the new position given by the camera
  # and compares that to the PPS using RSS.  It returns the RSS value between 
  # the PPS positions given and this new camera position calculated.
  
  if length(x) ~=12
    disp 'BAD INPUT!!!AAAAAAAAAAAAAAAAAAAAAA'
     
  end
  
  cameraposition = generate_IEC61217_Deq0(x(1:6));
  HawkTCP = generate_IEC61217_Deq0(x(7:12));
  
  obj = 0;
  # alright which camera are we optimizing, here
  solverfield = evalin('base','solverfield');
  # and what's the name of the struct?
  structname = evalin('base','solverstruct');
  # create new string for new cameravalues
  
  # aaaand let's grab the struct we're optimizing
  
  fixedppspositions = evalin('base',structname);
  
  # generate new positions for each position, calculate RSS, and sum.
  for i=1:length(fixedppspositions)
    
    fixedppspositions(i).newvalue = cameraposition*fixedppspositions(i).(solverfield);
    
    obj = obj + calcRSSNoAngles(fixedppspositions(i).newvalue,(generate_IEC61217_Deq0(fixedppspositions(i).correctedppsposition)*HawkTCP));
  
  endfor
  
  
  
endfunction
