function obj = cameraToLaserObjectiveFunction(x)
  
  # here's the phi function for optimization.
  
  # take the input x vector and check that it's a length 6 vector.
  
  if length(x) ~=6
    disp 'BAD INPUT!!!'
     
  end
  
  #ok, it's a good vector.  convert vector to euler.
  
  fourbyx =  generate_IEC61217_Deq0(x(1:6));
 
  obj = 0;
  
  # alright which camera are we optimizing, here
  solverfield = evalin('base','solverfield');
  # grab globals.
  fixedppspositions = evalin('base','fixedppspositions');

  
  # okay, calculate new positions.
  for i = 1: length(fixedppspositions)
    fixedppspositions(i).(solverfield) = fourbyx * fixedppspositions(i).(solverfield);
    
  end

  [meritvalue,meritarray]=calcMeritFunctionArbitrary(fixedppspositions);
  obj = meritvalue;
  
endfunction
