function obj = Scratchrotator(x)


  # Scratchrotator is an objective function that takes in existing data and spits out 
  # the worst case RSS difference in each position function.  This is primarily useful
  # when rotating about a single point and checking runout (it's effectively a runout
  # merit function).  Angular error dominates here.
  
  
  newbasis = x;

  camerabasisshift = evalin('base','camerabasisshift');
  solvedaquilafalco = evalin('base','solvedaquilafalco');
  
  
  obj = 0;
  # alright which camera are we optimizing, here
  
  # and what's the name of the struct?
  structname = evalin('base','structname');
  fixedppspositions = evalin('base',structname);

  for i = 1:length(fixedppspositions)
    fixedppspositions(i).rawaquilas = (generate_IEC61217_Deq0(camerabasisshift) * generate_IEC61217_Deq0(solvedaquilafalco(1:6)))^-1*fixedppspositions(i).aquila;
  endfor



    clear vals
    for i = 1:length(fixedppspositions)
      temp = rt_to_IEC61217_Deq0_params(generate_IEC61217_Deq0(newbasis) * fixedppspositions(i).rawaquilas)*generate_IEC61217_Deq0(newbasis(7:12))^-1;
      
      vals(i,1) = temp(1);
      vals(i,2) = temp(2);
      vals(i,3) = temp(3);
      vals(i,4) = temp(4);
      vals(i,5) = temp(5);
    endfor

## the following section can be used for evaluation if required.
##
##    figure('Name',[datestr(now)," ",mat2str(newbasis)])
##    subplot(3,1,1)
##    plot([0:rad2deg(howfar)/(length(vals)-1):rad2deg(howfar)],vals(:,1))
##    legend('x val')
##    xlabel('rz angle')
##    ylabel('value in m')
##    subplot(3,1,2)
##
##    plot([0:rad2deg(howfar)/(length(vals)-1):rad2deg(howfar)],vals(:,2))
##    legend('y val')
##    xlabel('rz angle')
##    ylabel('value in m')
##    subplot(3,1,3)
##
##    plot([0:rad2deg(howfar)/(length(vals)-1):rad2deg(howfar)],vals(:,3))
##    legend('z val')
##    xlabel('rz angle')
##    ylabel('value in m')
    
   obj = sqrt((max(vals(:,5))-min(vals(:,5)))^2+(max(vals(:,4))-min(vals(:,4)))^2+(max(vals(:,1))-min(vals(:,1)))^2+(max(vals(:,2))-min(vals(:,2)))^2+(max(vals(:,3))-min(vals(:,3)))^2);
    
    



