function obj = findCenterAngles(x)
  
  # here's the phi function for optimization to find the centerpoint of rotation versus an incoming set of IECdata.  LET'S GO.
  obj = 0;
  # alright which camera are we optimizing, here
  cameraselector = evalin('base','cameraselector');
  # and what's the name of the struct?
  structname = evalin('base','structname');
  # create new string for new cameravalues
  
  # aaaand let's grab the struct we're optimizing
  
  fixedppspositions = evalin('base',structname);
  
  # take the input x vector and check that it's a length 2 vector.  
  
  if length(x) ~=2
    disp 'BAD INPUT!!!'
     
  end
  
  #ok, it's a good vector!
  
  #ok, grab the old point as an arrayfun
  
  oldpoint = evalin('base','x2');
  oldpoint = [transpose(oldpoint),[0,0,0]];
  
  # convert array from IEC to matrix_type
  IECmatrix = generate_IEC61217_Deq0(oldpoint);
  
  # generate unit vector with x values
  unitvector = generate_IEC61217_Deq0([0,0,.1,x(1),x(2),0]);
  
  # ok, generate new point in space
  
  newpoint = rt_to_IEC61217_Deq0_params(IECmatrix*unitvector^-1 );
  
  
  # okay, generate lengths between the camera xyz IEC value versus the new point.
  
  for i = 1: length(fixedppspositions)
    IECVector = fixedppspositions(i).(cameraselector);
    xyzdistances(i) = sqrt((IECVector(1)-newpoint(1))^2+(IECVector(2)-newpoint(2))^2+(IECVector(3)-newpoint(3))^2);
    
  endfor
  
  #calculate merit function, which is to minimize the sum of differences between the mean and each point
  averagexyz = mean(xyzdistances);
  for i = 1: length(xyzdistances)
    obj = obj + sqrt((xyzdistances(i)-averagexyz)^2);

    
    #...done!  hyperstupid but It'll Do!
    
  endfor
  
  
  
  
  
  
endfunction
