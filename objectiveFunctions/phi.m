function obj = phi(x)
  
  # here's the phi function for optimization.
  
  # take the input x vector and check that it's a length 6 vector.
  
  if length(x) ~=6
    disp 'BAD INPUT!!!'
     
  end
  
  #ok, it's a good vector.  convert vector to euler.
  
  fourbyx = generate_transformation_matrix_eulerXYZ(x);
  
  # grab globals.
  capturedpoints = evalin('base','capturedpoints');
  falcocameraposition = evalin('base','falcocameraposition');
  
  # okay, calculate new positions.
  for i = 1: length(capturedpoints)
    capturedpoints(i).newfalco = fourbyx * falcocameraposition^-1 * capturedpoints(i).falco;
    
  end

  [meritvalue,meritarray]=calcMeritFunction(capturedpoints);
  obj = meritvalue;
  
endfunction
