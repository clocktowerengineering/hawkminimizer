function obj = findCenterPoint(x)
  
  # here's the phi function for optimization to find the centerpoint of rotation versus an incoming set of IECdata.  LET'S GO.
  obj = 0;
  # alright which camera are we optimizing, here
  cameraselector = [evalin('base','cameraselector')];
  # and what's the sruct we're optimizing, here
  structname = evalin('base','structname');
  # create new string for new cameravalues
  
  # aaaand let's grab the struct we're optimizing
  
  fixedppspositions = evalin('base',structname);
  
  # take the input x vector and check that it's a length 3 vector.  
  
  if length(x) ~=3
    disp 'BAD INPUT!!!'
     
  end
  
  #ok, it's a good vector!
  

  # okay, generate lengths between the camera xyz IEC value versus the provided vector.
  
  for i = 1: length(fixedppspositions)
    IECVector = fixedppspositions(i).([cameraselector,'IEC']);
    xyzdistances(i) = sqrt((IECVector(1)-x(1))^2+(IECVector(2)-x(2))^2+(IECVector(3)-x(3))^2);
    
  endfor
  
  #calculate merit function, which is to minimize the sum of differences between the mean and each point
  averagexyz = mean(xyzdistances);
  for i = 1: length(xyzdistances)
    obj = obj + sqrt((xyzdistances(i)-averagexyz)^2);
    #... okay, and since it isn't a single point that is the center of rotation, it's an entire fuckin axis, let's figure out a way to add the deltaZ in here
    
    obj = obj + sqrt((IECVector(3)-x(3))^2);
    
    #...done!  hyperstupid but It'll Do!
    
  endfor
  
  
  
  
  
  
endfunction
